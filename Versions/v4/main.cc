#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Geometry.h"
#include "Invalid.h"

using namespace std;
using namespace Geometry;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

// ----------------------------------- 80 ----------------------------------- //

int main (void)
try
{
	// Open output file.
	// -
	cout << "Please enter output file name: ";
	string name;
	cin >> name;
	ofstream os(name.c_str());
	if (!os)
		throw Invalid("Could not open file '" + name + "'!!!");
	name = "data.txt";
	// Open input file.
	// -
	ifstream is(name);
	if (!is)
		throw Invalid("Could not open '" + name + "' for writing!!!");
	vector<Point> original_points;
	// Read data.
	// -
	Point p;
	while (is >> p) original_points.push_back(p);
	// Print data.
	// -
	for (Point p : original_points) {
		os << p << endl;
	}
	// Read printed data.
	// -
	os.close();
	is.close();
	is.open(name);
	if (!is)
		throw Invalid("Could not open '" + name + "' for reading!!!");
	vector<Point> processed_points;
	while (is >> p) processed_points.push_back(p);
	cout << "Orig\tProcesd" << endl;
	for (unsigned int i = 0;
	     i < min(original_points.size(), processed_points.size()); ++i) {
		cout << original_points[i] << "\t" << processed_points[i] << endl;
	}
	if (original_points.size() != processed_points.size()) {
		cerr << "Something went wrong, vector sizes differ!!!" << endl;
		return 0;
	}
	for (unsigned int i = 0; i < original_points.size(); ++i) {
		if (original_points[i] != processed_points[i])
			cerr << "Points differ @ " << i << "." << endl;
	}
	return 0;
} catch (exception& e)
{
	cerr << "Built-in Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (Invalid& i)
{
	cerr << "Custom Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << i.what() << "\"" << endl;
	return 2;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return 3;
}
