/*
	calculator08buggy.h

	Grammatikh:

	Ypologismow:
		Entolh
		Ektypvsh
		Bohueia
		Egkataleich
		Entolh ypologismoy

	Entolh:
		Dhlvsh
		Ekfrash

	Dhlvsh:
		let Onoma = Ekfrash
		# Onoma = Ekfrash

	Ektypvsh:
		;
		<Enter>

	Bohueia:
		help

	Egkataleich:
		quit
		exit

	Ekfrash:
		Orow
		Ekfrash + Orow
		Ekfrash - Orow
	Orow:
		Prvtarxikow orow
		Orow * Prvtarxikow orow
		Orow / Prvtarxikow orow
		Orow % Prvtarxikow orow
	Prvtarxikow orow:
		Ariumow
		(Ekfrash)
	-Prvtarxikow orow:
	+Prvtarxikow orow:
	Ariumow:
		floating point literal

	H eisodow proerxetai apo to cin mesa apo to Token_stream poy onomazetai ts.
*/

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include "help.h"
// ^print_help()
// -
