#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include "Geometry.h"
#include "Invalid.h"

using namespace std;
using namespace Geometry;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

// ----------------------------------- 80 ----------------------------------- //

int main (void)
try
{
	// Open input file.
	// -
	const string name = "data.txt";
	ifstream is(name);
	if (!is)
		throw Invalid("Could not open '" + name + "' for writing!!!");
	vector<int> integers;
	// Read data.
	// -
	int i;
	while (true) {
		if (is >> i) {
			integers.push_back(i);
			cout << "Read : " << i << endl;
			continue;
		}
		if (is.eof() || is.bad()) {
			break;
		}
		if (is.fail()) {
			cerr << "Bad input! Ignoring..." << endl;
			is.clear();
			is.ignore();
			continue;
		}
	}
	if (!is && !is.eof())
		cerr << "Some error occured!!!" << endl;
	// Print data.
	// -
	long long sum = 0;
	for (int i : integers) {
		sum += i;
		cout << i << endl;
	}
	cout << "Sum = " << sum << endl;
	return 0;
} catch (exception& e)
{
	cerr << "Built-in Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (Invalid& i)
{
	cerr << "Custom Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << i.what() << "\"" << endl;
	return 2;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return 3;
}
