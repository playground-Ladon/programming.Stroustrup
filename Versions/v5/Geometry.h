#include <istream>
#include "Invalid.h"

namespace Geometry {
class Point {
	int x, y;
public:
	Point (void);
	Point (const int& x, const int& y);

	bool operator != (const Point& p) const { return x != p.x || y == p.y; }
	friend std::istream& operator >> (std::istream& is, Point& p);
	friend std::ostream& operator << (std::ostream& is, const Point& p);
};
} // Geometry

