#include "Geometry.h"

//------------------------------------------------------------------------------

namespace Geometry {

//------------------------------------------------------------------------------

Point::Point (void) : x(0), y(0) { }

//------------------------------------------------------------------------------

Point::Point (const int& x, const int& y) : x(x), y(y) { }

//------------------------------------------------------------------------------

std::istream& operator>> (std::istream& is, Geometry::Point& p)
{
	char a,b,c;
	if ((is >> a >> p.x >> b >> p.y >> c) &&
	    !(a == '(' && b == ',' && c == ')'))
		throw Invalid("Invalid format!!!");
	return is;
}

//------------------------------------------------------------------------------

std::ostream& operator<< (std::ostream& os, const Geometry::Point& p)
{
	return os << '(' << p.x << ',' << p.y << ')';
}

}
