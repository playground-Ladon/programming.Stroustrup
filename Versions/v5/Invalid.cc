#include "Invalid.h"

using namespace std;

//------------------------------------------------------------------------------

Invalid::Invalid (void) : msg("Unknown exception!") { }

//------------------------------------------------------------------------------

Invalid::Invalid (const string& str) : msg(str) { }

//------------------------------------------------------------------------------

const string Invalid::what (void ) const
{
	return msg;
}
