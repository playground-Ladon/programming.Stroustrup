#include "main.h"

using namespace std;

// ----------------------------------- 80 ----------------------------------- //

vector<string> split (const string& line, const string& white)
{
	vector<string> v;
	for (auto i = 0, start = 0; i < line.size(); ++i) {
		static string s;
		if (isspace(line[i]) || white.find(line[i]) != string::npos) {
			if (i > start + 1) {
				// Ignore previous <space>.
				// -
				++start;
				s = line.substr(start, i - start);
				v.push_back(s);
			}
			start = i;
		}
	}
	return v;
}

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	// Check arguments.
	// -
	if (argc != 3)
		throw runtime_error("2 arguments expected!");
	// Open input file.
	// -
	const char * ifile = argv[1];
	ifstream is(ifile);
	if (!is)
		throw runtime_error("Could not open file " + string(ifile) + "!!!");
	// Copy data.
	// -
	for (string line; getline(is,line); getline(is, line)) {
		static vector<string> v;
		static char* white = argv[2];
		v = split(line,white);
		for (string s : v) {
			cout << " :: " << s << "'" << endl;
		}
	}
	return 0;
} catch (exception& e)
{
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
