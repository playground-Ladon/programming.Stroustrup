#include "main.h"

using namespace std;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

bool isvowel (const char& c)
{
	switch (c) {
	case 'a':
	case 'e':
	case 'i':
	case 'o':
	case 'u':
	case 'y':
		return true;
	default:
		return false;
	}
}

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	cout << "Enter integer numbers in different bases (dec, oct, hex)." << endl <<
	     ": ";
	string s;
	while (cin >> s) {
		static bool invalid;
		invalid = false;
		for (char c : s)
			// Ignore invalid.
			// -
			if (!isdigit(c) && c != 'x') {
				invalid = true;
				break;
			}
		if (invalid)
			continue;
		switch (s.size()) {
		case 0:
			continue;
		case 1:
		case 2:
			cout << s << " decimal is converted to " << stoi(s) <<
			     " decimal." << endl;
			continue;
		default:
			// More than or equal to 3 digits.
			// -
			switch (s[0]) {
			case '0':
				switch (s[1]) {
				case 'x':
				case 'X':
					cout << s << " hexadecimal is converted to " <<
					     stoi(s,0,16) << " decimal." << endl;
					continue;
				default:
					// Octal.
					// -
					cout << s << " octal is converted to " <<
					     stoi(s,0,8) << " decimal." << endl;
					continue;
				}
			default:
				// Decimal.
				// -
				cout << s << " decimal is converted to " << stoi(s) <<
				     " decimal." << endl;
				continue;
			}
		}
	}
	return 0;
} catch (exception& e)
{
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
