#include "main.h"

using namespace std;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	// Open file.
	// -
	cout << "Please enter filename: ";
	string filename;
	cin >> filename;
	ifstream ifile(filename);
	if (!ifile) {
		cerr << "@ main < Error opening '" << filename << "'!!!" << endl;
		return 1;
	}
	// Read file.
	// -
	long i;
	long long sum = 0;
	while (ifile.good()) {
		if (ifile >> i) {
			sum += i;
		} else {
			ifile.clear();
			ifile.ignore();
		}
	}
	cout << "Sum is: " << sum << endl;
	return 0;
} catch (exception& e)
{
	cerr << "Built-in Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
