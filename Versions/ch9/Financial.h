#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>

#include "Invalid.h"

//------------------------------------------------------------------------------

namespace Financial {

//------------------------------------------------------------------------------

enum Currency {USD, EUR, GBP};

//------------------------------------------------------------------------------

class Money {

	long cents;
	Currency currency;

	Money (const long& cents);
	Money (const long& cents, const Currency& c);

	operator long () const;

	const Currency& default_currency (void) const;
public:
	// Constructors.
	// -
	Money (void);
	Money (const Currency& c);
	Money (const double& d);
	Money (const double& d, const Currency& c);
	Money (const std::string& s);
	Money (const std::string& s, const Currency& c);

	long get_cents (void) const;

	Money& operator= (const Money& m);
	Money& operator= (const std::string& s);

	Money operator+ (const Money& m);
	Money operator+ (const std::string& s);
	Money operator- (const Money& m);
	Money operator- (const std::string& s);
};

//------------------------------------------------------------------------------

std::ostream& operator<< (std::ostream& os, const Money& m);

} // Financial

