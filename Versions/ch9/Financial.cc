#include "Financial.h"

using namespace std;
using namespace Financial;

//------------------------------------------------------------------------------

Money::Money (const long& cents) : Money(cents, default_currency()) { }

//------------------------------------------------------------------------------

Money::Money (const long& cents, const Currency& c) : cents(cents), currency(c) { }

//------------------------------------------------------------------------------

Money::operator long () const
{
	return cents;
}

//------------------------------------------------------------------------------

const Currency& Money::default_currency (void) const
{
	static const Currency def = EUR;
	return def;
}

//------------------------------------------------------------------------------

Money::Money (void) : Money(default_currency()) { }

//------------------------------------------------------------------------------

Money::Money (const Currency& c) : cents(0), currency(c) { }

//------------------------------------------------------------------------------

Money::Money (const double& d) : Money(d, default_currency()) { }

//------------------------------------------------------------------------------

Money::Money (const double& d, const Currency& c) : currency(c)
{
	double dd;
	cents = dd = round(d * 100);
	if (cents != dd) {
		throw Invalid("Money does not fit!" );
	}
}

//------------------------------------------------------------------------------

Money::Money (const string& s) : Money(s, default_currency()) { }

//------------------------------------------------------------------------------

Money::Money (const string& s, const Currency& c) : Money(stod(s), c) { }

//------------------------------------------------------------------------------

long Money::get_cents (void) const
{
	return cents;
}

//------------------------------------------------------------------------------

Money& Money::operator= (const Money& m)
{
	cents = m.cents;
	return *this;
}

//------------------------------------------------------------------------------

Money& Money::operator= (const string& s)
{
	return operator= (Money(s));
}

//------------------------------------------------------------------------------

Money Money::operator+ (const Money& m)
{
	return Money(this->cents + m.cents);
}

//------------------------------------------------------------------------------

Money Money::operator+ (const string& s)
{
	return operator+ (Money(s));
}

//------------------------------------------------------------------------------

Money Money::operator- (const Money& m)
{
	return Money(this->cents - m.cents);
}

//------------------------------------------------------------------------------

Money Money::operator- (const string& s)
{
	return operator- (Money(s));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

ostream& Financial::operator<< (ostream& os, const Money& m)
{
	return os << m.get_cents() / 100 << "." << setfill('0') << setw(2) << m.get_cents() % 100;
}
