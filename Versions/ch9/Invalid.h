#pragma once

#include <string>

class Invalid {
	std::string msg;
public:
	Invalid (void);
	Invalid (const std::string& str);

	const std::string what (void) const;
};
