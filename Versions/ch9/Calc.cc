#include "Calc.h"

namespace Calc
{

//------------------------------------------------------------------------------

u64 Rational::gcd (void)
{
	// Implementing a version of Euclid's algorithm...
	// -
	u64 num = numerator;
	u64 denom = denominator;

	// Until one is zero...
	// -
	while (num && denom) {
		if (num > denom) {
			num %= denom;
		} else {
			denom %= num;
		}
	}

	return num + denom;
}

//------------------------------------------------------------------------------

void Rational::reduce (void)
{
	for (u64 i = gcd(); i != 1; i = gcd()) {
		numerator /= i;
		denominator /= i;
	}
}

//------------------------------------------------------------------------------

Rational::Rational (const u64& num, const u64& denom) : numerator(num),
	denominator(denom)
{
	if (denom == 0)
		throw Invalid("Denominator is zero.");
	reduce();
}

//------------------------------------------------------------------------------

Rational::Rational (void) : Rational::Rational (0,1) { }

//------------------------------------------------------------------------------

u64 Rational::getnum (void) const
{
	return numerator;
}

//------------------------------------------------------------------------------

u64 Rational::getdenom (void) const
{
	return denominator;
}

//------------------------------------------------------------------------------

Rational::operator long double () const
{
	return (long double) numerator / denominator;
}

//------------------------------------------------------------------------------

Rational& Rational::operator= (const string& s)
{
	// numerator / denominator
	// -
	u64 p[2];
	int v = sscanf(s.c_str(), "%llu/%llu", p, p + 1);
	if (v != 2)
		throw Invalid("Rational: Bad assignment: " + s);

	return *this = Rational(p[0], p[1]);
}

//------------------------------------------------------------------------------

Rational Rational::operator- (void) const
{
	return Rational(-numerator, denominator);
}

//------------------------------------------------------------------------------

Rational Rational::operator+ (const Rational& r) const
{
	return Rational(numerator * r.denominator + denominator * r.numerator,
	                denominator * r.denominator);
}

//------------------------------------------------------------------------------

Rational Rational::operator- (const Rational& r) const
{
	return operator+(-r);
}

//------------------------------------------------------------------------------
Rational Rational::operator* (const Rational& r) const
{
	return Rational(numerator * r.numerator, denominator * r.denominator);
}

//------------------------------------------------------------------------------

Rational Rational::operator/ (const Rational& r) const
{
	return Rational(numerator * r.denominator, denominator * r.numerator);
}

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const Rational& r)
{
	return os << r.getnum() << "/" << r.getdenom();
}

//------------------------------------------------------------------------------
} // Calc
