#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

#include "Chrono.h"
#include "Calc.h"
#include "Financial.h"
#include "Invalid.h"

using namespace std;
using namespace Chrono;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //
namespace Literature
{

enum Genre {lit, his, mag, bio, childen};

class Book
{
	string title;
	string author;
	string isbn; // int-int-int-char //
	string copyright;
	Genre genre;
public:
	Book (cstr& title, cstr& author, cstr& isbn, cstr& copyright,
	      const Genre& genre);
	bool isvalid (cstr& isbn);
	bool operator== (const Book& q) const { return this->isbn == q.isbn; }
	bool operator!= (const Book& q) const { return !operator==(q); }

// replaced with constructor:
// -
//	void settitle (cstr& title) { this->title = title; }
//	void setauthor (cstr& author) { this->author = author; }
//	void setisbn (cstr& isbn) { this->isbn = isbn; }
//	void setcopyright (cstr& copyright) { this->copyright = copyright; }

	cstr gettitle (void) const { return title; }
	cstr getauthor (void) const { return author; }
	cstr getisbn (void) const { return isbn; }
	cstr getcopyright (void) const { return copyright; }
};

Book::Book (cstr& title, cstr& author, cstr& isbn, cstr& copyright,
            const Genre& genre)
{
	if (!isvalid(isbn))
		throw Invalid("Invalid ISBN entered: " + isbn + "!");
	this->title = title;
	this->author = author;
	this->isbn = isbn;
	this->copyright = copyright;
	this->genre = genre;
}

bool Book::isvalid (cstr& isbn)
{
	u32 u[3] = {(u32) -1};
	char c[2] = {'\0'};
	int v = sscanf(isbn.c_str(), "%u-%u-%u-%2c", u, u+1, u+2, c);
	if (v != 4 || (c[1] != '\0' && ! isspace(c[1])) ) {
		cout << "Invalid ISBN entered : " << isbn << endl;
		return false;
	}
	return true;
}

// ----------------------------------- 80 ----------------------------------- //
ostream& operator<< (ostream& os, const Book& b)
{
	os << "Title: " << b.gettitle() << endl;
	os << "ISBN : " << b.getisbn() << endl;
	return os;
}

// ----------------------------------- 80 ----------------------------------- //
class Patron
{
	string name;
	u32 id;
	double credits;
public:
	cstr& getname (void) const { return name; }
	const u32& getid (void) const { return id; }
	const double& getcredits (void) const { return credits; }

	void setcredits (const u32& credits) { this->credits = credits; }

	bool hasdebt (void) const { return credits < 0; }
	bool operator== (const Patron& p) const { return id == p.id; }
	bool operator!= (const Patron& p) const { return !operator==(p); }
};

// ----------------------------------- 80 ----------------------------------- //
class Library
{
	vector<Book> books;
	vector<Patron> patrons;
	struct Transaction {
		Patron patron;
		Book book;
		Chrono::Date date;
		Transaction (const Patron& p, const Book& b, const Date& d) : patron(p), book(b), date(d) { }
	};
	vector<Transaction> transactions;

	bool inbooks (const Book& book);
	bool inpatrons (const Patron& patron);
public:
	void addbook (const Book& b);
	void addpatron (const Patron& p);

	void givebook (const Patron& patron, const Book& book);
	vector<string> debtors (void) const;
};

bool Library::inbooks (const Book& book)
{
	for (const Book& b : books) {
		if (b == book)
			return true;
	}
	return false;
}

bool Library::inpatrons (const Patron& patron)
{
	for (const Patron& p : patrons) {
		if (p == patron)
			return true;
	}
	return false;
}

void Library::givebook (const Patron& patron, const Book& book)
{
	if (!inpatrons(patron)) {
		cout << "I'm sorry, this user is not yet registered!" << endl;
		return;
	}
	if (!inbooks(book)) {
		cout << "I'm sorry, it seems we do not have this book!" << endl;
		return;
	}
	if (patron.hasdebt()) {
		cout << "I'm sorry, you do not have enough credits since you are in dept!" <<
		     endl;
		return;
	}
	Transaction t(patron, book, Date());
	transactions.push_back(t);
}

vector<string> Library::debtors (void) const
{
	vector<string> v;
	for (const Patron& p : patrons) {
		if (p.hasdebt())
			v.push_back(p.getname());
	}
	return v;
}

} // Literature

// ----------------------------------- 80 ----------------------------------- //
// Book
// Genre
// -
using namespace Literature;
using namespace Calc;
using namespace Financial;

int main (int argc, char* argv[])
try {
	Money m(1.004),n("1e1000");
	cout << "m = " << m << endl << "n = " << n << endl;
	cout << (m = m + n) << endl;
	return 0;
} catch (std::invalid_argument& e) {
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << "Invalid argument @" << e.what() << "\"" << endl;
	return 4;
} catch (std::out_of_range& e) {
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << "Out of range! @" << e.what() << "\"" << endl;
	return 5;
} catch (exception& e) {
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 2;
} catch (Invalid& i) {
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << i.what() << "\"" << endl;
	return 1;
} catch (...) {
	cerr << "Unknown exception occured!!!" << endl;
	return 3;
}
