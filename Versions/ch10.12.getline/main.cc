#include "main.h"

using namespace std;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	// Check arguments.
	// -
	if (argc != 3) {
		// Print help.
		// -
		cout << argv[0] << " FILE WORD" << endl;
		return 1;
	}
	// Open file.
	// -
	ifstream ifile(argv[1]);
	if (!ifile) {
		cerr << "@ main < Error opening '" << argv[1] << "'!!!" << endl;
		return 1;
	}
	// Read file.
	// -
	char* w = argv[2];
	array <char, 81> l;
	int line = 0;
	while (ifile.good()) {
		ifile.getline(l.data(), l.max_size(), '\n');
		if (ifile.gcount() < l.max_size()) {
			++line;
		}
		if (strstr(l.data(), w) != NULL) {
			cout << line << ": " << l.data() << endl;
		}
	}
	return 0;
} catch (exception& e)
{
	cerr << "Built-in Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
