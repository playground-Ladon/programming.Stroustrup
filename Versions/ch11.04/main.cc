#include "main.h"

using namespace std;

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	char c;
	while (cin.get(c)) {
		cout << showbase << hex << c << " is :" << endl;
		if (isprint(c)) {
			cout << "\tprintable" << endl;
			if (isspace(c)) {
				cout << "\twhitespace" << endl;
			}
			if (isgraph(c)) {
				cout << "\tgraph" << endl;
				if (isalnum(c)) {
					cout << "\talphanumeric" << endl;
					if (isalpha(c)) {
						cout << "\talpharithmetic" << endl;
						if (isxdigit(c)) {
							cout << "\thexadecimal digit" << endl;
						}
						if (isupper(c)) {
							cout << "\tuppercase letter" << endl;
						}
						if (islower(c)) {
							cout << "\tlowercase letter" << endl;
						}
					}
					if (isdigit(c)) {
						cout << "\tdigit" << endl;
					}
				}
				if (ispunct(c)) {
					cout << "\tpuncuation" << endl;
				}
			}
		} else if (iscntrl(c)) {
			cout << "\tcontrol character" << endl;
		}
	}
	return 0;
} catch (exception& e)
{
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
