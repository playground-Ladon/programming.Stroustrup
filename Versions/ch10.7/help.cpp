#include "help.h"

void print_help(void)
{
	char message[] = "\
\"Space does not affect the outcome,\n\
expressions can be ended either with 'newline' or ';',\n\
help :			help\n\
quit :			exit, quit\"\n\
\n\
addition : 		5 + 5\n\
subtraction :		11 - 1\n\
multiplication :	2 * 5\n\
division :		100 / 10\n\
modulo :		22 % 12\n\
variable :		let x = 3\n\
variable :		# x = 3\n\
with variable :		x = x + 3\n\
constants :		pi; e;\n\
square root :		sqrt(4)\n\
exponential :		pow(base,exponent)\n";
	cout << message;
}
