/*
	calculator08buggy.cpp
*/

#include "calculator08buggy.h"

using namespace std;

void error(string s)
{
	cerr << s;
	exit ( 1);
}

void error(string p, string q)
{
	error(p+q);
}

int warn_prec(double p, int q)
{
	if (p > q)
		cout << "WARNING: underflow detected!\n";
	else if (p < q)
		cout << "WARNING: overflow detected!\n";
	return q;
}

int warn_prec(double p) {
	return warn_prec(p, (int) p);
}

struct Token {
	// Variables
	// -
	char kind;
	double value;
	string name;
	// Constructors
	// -
	Token(char ch) :kind(ch), value(0) { }
	Token(char ch, double val) :kind(ch), value(val) { }
	Token(char ch, string nam) :kind(ch), name(nam) {}
};

class Token_stream {
	bool full;
	Token buffer;
	istream& st;
public:
	Token_stream(istream& st) : full(0), buffer(0), st(st) {}
	Token_stream() : Token_stream(cin) {}
	Token get();
	void unget(Token t) { buffer=t; full=true; }

	void ignore(char);
};

// Aliases to make the code more readable
// -
const char let =	'L';
const char print =	';';
const char help =	'h';
const char number =	'8';
// aliases for +=, -=, *= ...
// -
const char plus_eq =	'a';
const char minus_eq =	'b';
const char mult_eq =	'c';
const char div_eq =	'd';
const char modul_eq =	'e';
//
const char kilos =	'k';
const char power =	'P';
const char sqroot = 	'R';
const char name	 =	'N';
const char quit =	'Q';

Token Token_stream::get()
{
	if (full) { full=false; return buffer; }
	char ch;
	// this is a hack to print on newline
	// -
	cin.get(ch);
	while (isspace(ch) && ch != '\n')
		cin.get(ch);
	//
	switch (ch) {
	// check for +=,-=,*=...
	// -
	case '+': case '-':
	case '*': case '/':
	case '%': {
		if (cin.get() == '=') {
			// overwrite 'ch', to be returned farther down
			// -
			switch (ch) {
			case '+':
				ch = plus_eq;
				break;
			case '-':
				ch = minus_eq;
				break;
			case '*':
				ch = mult_eq;
				break;
			case '/':
				ch = div_eq;
				break;
			case '%':
				ch = modul_eq;
				break;
			}
		} else {
			// else
			// it was a regular +,-,*...
			// return it normally
			// -
			cin.unget();
		}
	}
	case '(':
	case ')':
	case 'k':
	case ';':
	case '=':
	case ',':
		return Token(ch);
	case '.':
	case 'I': case 'V':
	case 'X': case 'L':
	case 'C': case 'D':
	case 'M':
	{	cin.unget();
		Roman_int roman;
		cin >> roman;
		return Token(number,roman.as_int());
	}
	case '\n':
		return Token(print);
	case '#':
		return Token(let);
	default:
		if (isalpha(ch)) {
			string s;
			s += ch;
			while(cin.get(ch) && (isalpha(ch) || isdigit(ch) || ch == '_')) s+=ch;
			cin.unget();
			if (s == "let") return Token(let);
			if (s == "quit" || s == "exit") return Token(quit);
			if (s == "help") return Token(help);
			if (s == "sqrt") return Token(sqroot);
			if (s == "pow") return Token(power);
			return Token(name,s);
		}
		error("Bad token");
	}
}

// Removes all chars, up to and including, 'c'
// -
void Token_stream::ignore(char c)
{
	if (full && c==buffer.kind) {
		full = false;
		return;
	}
	full = false;

	char ch;
	while (cin>>ch)
		if (ch==c) return;
}

// A variable for/by the user, holding a number
// -
struct Variable {
	bool is_const;
	string name;
	double value;
	Variable(string n, double v) :is_const(false), name(n), value(v) { }
	Variable(string n, double v, bool is_const) :is_const(is_const), name(n), value(v) { }
};

// A class to hold the user variables and the system
// defined constants. It is meant to replace the global
// vector<Variable> names.
// -
class Symbol_table {
private:
	vector<Variable> var_table;
public:
	double declare(string s, double d);
	double declare(string s, double d, bool c);
	double get(string s);
	bool is_declared(string s);
	void set(string s, double d);
} symbol_table;

// Does NOT check if it is declared already.
// -
double Symbol_table::declare(string s, double d)
{
	var_table.push_back(Variable(s,d));
}

double Symbol_table::declare(string s, double d, bool c)
{
	var_table.push_back(Variable(s,d,c));
}

// Returns the number contained in a variable
// -
double Symbol_table::get(string s)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) return var_table[i].value;
	error("get: undefined variable ",s);
}

bool Symbol_table::is_declared(string s)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) return true;
	return false;
}

// Assigns a number to a variable
// -
void Symbol_table::set(string s, double d)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) {
			if (var_table[i].is_const)
				error("Symbol_table::set() tried to assign to const!");
			var_table[i].value = d;
			return;
		}
	error("Undefined name at Symbol_table::set()",s);
}

// add constants
// -
void create_constants(void)
{
	symbol_table.declare("pi",3.141592,true);
	symbol_table.declare("e",2.27,true);
}

double expression(Token_stream& ts);

double primary(Token_stream& ts)
{
	Token t = ts.get();
	switch (t.kind) {
	case power: {
		t = ts.get();
		if (t.kind != '(') error ("pow without '('!!");
		double base = expression(ts);
		t = ts.get();
		if (t.kind != ',') error ("pow without middle ','!!");
		double exp = expression(ts);
		t = ts.get();
		if (t.kind != ')') error ("pow without closing ')'!!");
		int exp_int = exp;
		if (exp_int != exp) error ("non-integer power requested!");
		return warn_prec(pow(base,exp),pow((int) base, (int) exp));
	}
	case sqroot: {
		t = ts.get();
		if (t.kind != '(') error ("sqrt without '('!!");
		double d = expression(ts);
		t = ts.get();
		if (t.kind != ')') error ("sqrt without closing ')'!!");
		if (d < 0) error ("square root of negative number requested!");
		return warn_prec(sqrt(d),sqrt((int) d));
	}
	case '(':
	{	double d = expression(ts);
		t = ts.get();
		if (t.kind != ')') error("')' expected");
		return warn_prec(d);
	}
	case '-':
		return - primary(ts);
	case number:
		return warn_prec(t.value);
	case name: {
		Token tmpt = ts.get();
		switch (tmpt.kind) {
		// variable takes new value
		// -
		case '=': {
			double d = expression(ts);
			symbol_table.set(t.name,d);
			return d;
		}
		// variable takes new value
		// -
		case plus_eq: case minus_eq:
		case mult_eq: case div_eq:
		case modul_eq: {
			double d = expression(ts);
			double var = symbol_table.get(t.name);
			switch (tmpt.kind) {
			case plus_eq:
				var += d;
				break;
			case minus_eq:
				var -= d;
				break;
			case mult_eq:
				var *= d;
				break;
			case div_eq:
				var = warn_prec(var/d,(int) var/ (int) d);
				break;
			case modul_eq:
				var = (int) var % (int) d;
				break;
			default:
				break;
			}
			// I put it here, in case i forgot to check some case
			// -
			var = warn_prec(var);
			//
			symbol_table.set(t.name,var);
			return var;
		}
		// variable is returned
		// -
		default :
			ts.unget(tmpt);
			return symbol_table.get(t.name);
		}
	}
	default:
		error("primary expected");
	}
}

double term(Token_stream& ts)
{
	double left = primary(ts);
	while(true) {
		Token t = ts.get();
		switch(t.kind) {
		case '*':
			left *= primary(ts);
			break;
		case 'k':
			left *= 1000;
			break;
		case '/':
		{	double d = primary(ts);
			if (d == 0) error("divide by zero");
			left = warn_prec(left/d,(int) left / (int) d);
			break;
		}
		default:
			ts.unget(t);
			return warn_prec(left);
		}
	}
}

double expression(Token_stream& ts)
{
	double left = term(ts);
	while(true) {
		Token t = ts.get();
		switch(t.kind) {
		case '+':
			left += term(ts);
			break;
		case '-':
			left -= term(ts);
			break;
		default:
			ts.unget(t);
			return left;
		}
	}
}

double declaration(Token_stream& ts)
{
	Token t = ts.get();
	if (t.kind != name) error ("name expected in declaration");
	string name = t.name;
	if (symbol_table.is_declared(name)) error(name, " declared twice");
	Token t2 = ts.get();
	if (t2.kind != '=') error("= missing in declaration of " ,name);
	double d = expression(ts);
	symbol_table.declare(name,d);
	return d;
}

double statement(Token_stream& ts)
{
	Token t = ts.get();
	switch(t.kind) {
	case let:
		return declaration(ts);
	default:
		ts.unget(t);
		return expression(ts);
	}
}

// Remove all chars before the next computation
// -
void clean_up_mess(Token_stream& ts)
{
	ts.ignore(print);
}

const string prompt = "> ";
const string result = "= ";

void calculate(Token_stream& ts)
{
	while(true) try {
		cout << prompt;
		Token t = ts.get();
		// Remove concequtive 'print' chars.
		// -
		while (t.kind == print) t=ts.get();
		if (t.kind == quit) return;
		if (t.kind == help) {
			print_help();
			continue;
		}
		ts.unget(t);
		cout << result << statement(ts) << endl;
	}
	catch(runtime_error& e) {
		cerr << e.what() << endl;
		clean_up_mess(ts);
	}
}

int main()
	try {
		Token_stream ts;
		create_constants();
		calculate(ts);
		return 0;
	}
	catch (exception& e) {
		cerr << "exception: " << e.what() << endl;
		char c;
		while (cin >>c&& c!=';') ;
		return 1;
	}
	catch (...) {
		cerr << "exception\n";
		char c;
		while (cin>>c && c!=';');
		return 2;
	}
