
/*
	calculator08buggy.cpp

	Helpful comments removed.

	We have inserted 3 bugs that the compiler will catch and 3 that it won't.
*/

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
void error(string s)
{
	cerr << s;
	exit ( 1);
}

void error(string p, string q)
{
	error(p+q);
}

struct Token {
	// Variables
	// -
	char kind;
	double value;
	string name;
	// Constructors
	// -
	Token(char ch) :kind(ch), value(0) { }
	Token(char ch, double val) :kind(ch), value(val) { }
	Token(char ch, string nam) :kind(ch), name(nam) {}
};

class Token_stream {
	bool full;
	Token buffer;
public:
	Token_stream() :full(0), buffer(0) { }

	Token get();
	void unget(Token t) { buffer=t; full=true; }

	void ignore(char);
};

// Aliases to make the code more readable
// -
const char let =	'L';
const char quit =	'Q';
const char print =	';';
const char number =	'8';
const char kilos =	'k';
const char power =	'P';
const char sqroot = 	'R';
const char name	 =	'a';

Token Token_stream::get()
{
	if (full) { full=false; return buffer; }
	char ch;
	cin >> ch;
	switch (ch) {
	case '(':
	case ')':
	case '+':
	case '-':
	case '*':
	case 'k':
	case '/':
	case '%':
	case ';':
	case '=':
	case ',':
		return Token(ch);
	case '.':
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	{	cin.unget();
		double val;
		cin >> val;
		return Token(number,val);
	}
	case '#':
		return Token(let);
	case 'q':
		return Token(quit);
	default:
		if (isalpha(ch)) {
			string s;
			s += ch;
			while(cin.get(ch) && (isalpha(ch) || isdigit(ch) || ch == '_')) s+=ch;
			cin.unget();
			if (s == "let") return Token(let);
			if (s == "quit" || s == "exit") return Token(quit);
			if (s == "sqrt") return Token(sqroot);
			if (s == "pow") return Token(power);
			return Token(name,s);
		}
		error("Bad token");
	}
}

// Removes all chars, up to and including, 'c'
// -
void Token_stream::ignore(char c)
{
	if (full && c==buffer.kind) {
		full = false;
		return;
	}
	full = false;

	char ch;
	while (cin>>ch)
		if (ch==c) return;
}

// A variable for/by the user, holding a number
// -
struct Variable {
	bool is_const;
	string name;
	double value;
	Variable(string n, double v) :is_const(false), name(n), value(v) { }
	Variable(string n, double v, bool is_const) :is_const(is_const), name(n), value(v) { }
};

// Local variable storage
// -
vector<Variable> names;

// A class to hold the user variables and the system
// defined constants. It is meant to replace the global
// vector<Variable> names.
// -
class Symbol_table {
private:
	vector<Variable> var_table;
public:
	double declare(string s, double d);
	double get(string s);
	bool is_declared(string s);
	void set(string s, double d);
} symbol_table;

// Does NOT check if it is declared already.
// -
double Symbol_table::declare(string s, double d)
{
	var_table.push_back(Variable(s,d));
}

double Symbol_table::get(string s)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) return var_table[i].value;
	error("get: undefined variable ",s);
}

bool Symbol_table::is_declared(string s)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) return true;
	return false;
}

void Symbol_table::set(string s, double d)
{
	for (int i = 0; i<var_table.size(); ++i)
		if (var_table[i].name == s) {
			if (var_table[i].is_const)
				error("set_value() tried to assign to const!");
			var_table[i].value = d;
			return;
		}
	error("set: undefined name ",s);
}

// add constants
// -
void create_constants(void)
{
	names.push_back(Variable("pi",3.141592,true));
	names.push_back(Variable("e",2.27,true));
}

// Returns the number contained in a variable
// -
double get_value(string s)
{
	for (int i = 0; i<names.size(); ++i)
		if (names[i].name == s) return names[i].value;
	error("get: undefined variable ",s);
}

// Assigns a number to a variable
// -
void set_value(string s, double d)
{
	for (int i = 0; i<names.size(); ++i)
		if (names[i].name == s) {
			if (names[i].is_const)
				error("set_value() tried to assign to const!");
			names[i].value = d;
			return;
		}
	error("set: undefined name ",s);
}

double expression();

// update an existing variable with a new value
// -
/*
double update_value(string s)
{
	for (int i = 0; i<names.size(); ++i)
		if (names[i].name == s) {
			double d = expression();
			names[i].value = d;
			return d;
		}
	error("update: undefined name ",s);
}
*/
// Checks if a variable is declared
// -
bool is_declared(string s)
{
	for (int i = 0; i<names.size(); ++i)
		if (names[i].name == s) return true;
	return false;
}

Token_stream ts;

double primary()
{
	Token t = ts.get();
	switch (t.kind) {
	case power: {
		t = ts.get();
		if (t.kind != '(') error ("pow without '('!!");
		double base = expression();
		t = ts.get();
		if (t.kind != ',') error ("pow without middle ','!!");
		double exp = expression();
		t = ts.get();
		if (t.kind != ')') error ("pow without closing ')'!!");
		int exp_int = exp;
		if (exp_int != exp) error ("non-integer power requested!");
		return pow(base,exp);
	}
	case sqroot: {
		t = ts.get();
		if (t.kind != '(') error ("sqrt without '('!!");
		double d = expression();
		t = ts.get();
		if (t.kind != ')') error ("sqrt without closing ')'!!");
		if (d < 0) error ("square root of negative number requested!");
		return sqrt(d);
	}
	case '(':
	{	double d = expression();
		t = ts.get();
		if (t.kind != ')') error("')' expected");
		return d;
	}
	case '-':
		return - primary();
	case number:
		return t.value;
	case name: {
		Token tmpt = ts.get();
		if (tmpt.kind == '=') {
			double d = expression();
			set_value(t.name,d);
			return d;
		} else {
			ts.unget(tmpt);
			return get_value(t.name);
		}
	}
	default:
		error("primary expected");
	}
}

double term()
{
	double left = primary();
	while(true) {
		Token t = ts.get();
		switch(t.kind) {
		case '*':
			left *= primary();
			break;
		case 'k':
			left *= 1000;
			break;
		case '/':
		{	double d = primary();
			if (d == 0) error("divide by zero");
			left /= d;
			break;
		}
		default:
			ts.unget(t);
			return left;
		}
	}
}

double expression()
{
	double left = term();
	while(true) {
		Token t = ts.get();
		switch(t.kind) {
		case '+':
			left += term();
			break;
		case '-':
			left -= term();
			break;
		default:
			ts.unget(t);
			return left;
		}
	}
}

double declaration()
{
	Token t = ts.get();
	if (t.kind != name) error ("name expected in declaration");
	string name = t.name;
	if (is_declared(name)) error(name, " declared twice");
	Token t2 = ts.get();
	if (t2.kind != '=') error("= missing in declaration of " ,name);
	double d = expression();
	names.push_back(Variable(name,d));
	return d;
}

double statement()
{
	Token t = ts.get();
	switch(t.kind) {
	case let:
		return declaration();
	default:
		ts.unget(t);
		return expression();
	}
}

// Remove all chars before the next computation
// -
void clean_up_mess()
{
	ts.ignore(print);
}

const string prompt = "> ";
const string result = "= ";

void calculate()
{
	while(true) try {
		cout << prompt;
		Token t = ts.get();
		// Remove concequtive 'print' chars.
		// -
		while (t.kind == print) t=ts.get();
		if (t.kind == quit) return;
		ts.unget(t);
		cout << result << statement() << endl;
	}
	catch(runtime_error& e) {
		cerr << e.what() << endl;
		clean_up_mess();
	}
}

int main()

	try {
		create_constants();
		calculate();
		return 0;
	}
	catch (exception& e) {
		cerr << "exception: " << e.what() << endl;
		char c;
		while (cin >>c&& c!=';') ;
		return 1;
	}
	catch (...) {
		cerr << "exception\n";
		char c;
		while (cin>>c && c!=';');
		return 2;
	}
