#include <algorithm>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>

class Numeral
{
	unsigned char symbol;
	unsigned short value;
	static const char symbols [];
	static const unsigned short symbol_values [];
	static const unsigned char symbols_len;

	bool is_valid (const char& c) const;
	unsigned short num_to_i (const char& c) const;
public:
	Numeral (void);
	Numeral (const char& c);

	unsigned char as_char (void) const;
	unsigned short as_int (void) const;

	bool operator < (const Numeral& n) const;
	bool operator > (const Numeral& n) const;

	static char next_scale (const char& c);
};

class Roman_int
{
	int value;
	static const char symbols [];
	static const unsigned short values [];

	bool is_valid (const std::string& s) const;
	unsigned short symbol_value (const char& c) const;
public:
	Roman_int (void);
//	Roman_int (const int& i);
	Roman_int (const std::string& s);

	int as_int (void) const;

	friend std::istream& operator >> (std::istream& is, Roman_int& r);
	friend std::ostream& operator << (std::ostream& os, const Roman_int& r);
};
