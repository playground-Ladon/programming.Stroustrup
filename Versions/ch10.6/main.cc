#include "main.h"

using namespace std;

typedef const string cstr;
typedef unsigned int u32;

// ----------------------------------- 80 ----------------------------------- //

// ----------------------------------- 80 ----------------------------------- //

int main (void)
try
{
	string s;
	while (true) {
		cin >> s;
		cout << Roman_int(s).as_int() << endl;
	}
	return 0;
} catch (exception& e)
{
	cerr << "Built-in Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
