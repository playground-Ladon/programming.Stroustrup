#include "Roman_int.h"

using namespace std;

typedef unsigned char byte;

const char Numeral::symbols[] = "IVXLCDM";
const unsigned short Numeral::symbol_values [] = {1,5,10,50,100,500,1000};
const unsigned char Numeral::symbols_len = strlen(Numeral::symbols);

const char Roman_int::symbols [] = "IVXLCDM";
const unsigned short Roman_int::values [] = {1,5,10,50,100,500,1000};

//------------------------------------------------------------------------------

bool Numeral::is_valid (const char& c) const
{
	// Compare all characters
	// -
	static int i;
	for (i = 0; i < symbols_len; ++i)
		if (c == symbols[i])
			break;
	return (i != symbols_len);
}

//------------------------------------------------------------------------------

unsigned short Numeral::num_to_i (const char& c) const
{
	for (int i = 0; i < symbols_len; ++i)
		if (c == symbols[i])
			return symbol_values[i];
	throw invalid_argument("Numeral::num_to_i: Not a numeral!!!");
}

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

Numeral::Numeral (void) : symbol('\0'), value(0) { }

//------------------------------------------------------------------------------

Numeral::Numeral (const char& c)
{
	if (!is_valid(c))
		throw invalid_argument("Not a roman numeral!!!");
	value = num_to_i (c);
	symbol = c;
}

//------------------------------------------------------------------------------

unsigned char Numeral::as_char (void) const
{
	return symbol;
}

//------------------------------------------------------------------------------

unsigned short Numeral::as_int (void) const
{
	return value;
}

//------------------------------------------------------------------------------

bool Numeral::operator < (const Numeral& n) const
{
	return (this->value < n.value);
}

//------------------------------------------------------------------------------

bool Numeral::operator > (const Numeral& n) const
{
	return (this->value > n.value);
}

//------------------------------------------------------------------------------

char Numeral::next_scale (const char& c)
{
	int i;
	for (i = 0; i < symbols_len; ++i)
		if (c == symbols[i])
			break;
	if (i == symbols_len)
		throw invalid_argument("Numeral::next_scale: Bad char!!!");
	if (symbols_len && i == symbols_len - 1)
		throw invalid_argument("Numeral::next_scale: Got maximum symbol!!!");
	return symbols[i + 1];
}

//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------//
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

bool Roman_int::is_valid (const string& s) const
{
	// Compare all characters to builtin symbols []
	// -
	static int len = strlen(symbols);
	for (char c : s) {
		static int i;
		for (i = 0; i < len; ++i)
			if (c == symbols[i])
				break;
		if (i == len)
			return false;
	}
	return true;
}

//------------------------------------------------------------------------------

unsigned short Roman_int::symbol_value (const char& c) const
{
	for (byte i = 0; i < strlen(symbols); ++i) {
		if (c == symbols[i])
			return values[i];
	}
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

Roman_int::Roman_int (void) : value(0)
{
}

//------------------------------------------------------------------------------

//Roman_int::Roman_int (const int& i)
//{
//}

//------------------------------------------------------------------------------

Roman_int::Roman_int (const string& s)
{
	if (!is_valid(s))
		throw invalid_argument("Not a roman numeral: '" + s + "'!!!");
	value = 0;
	char last = 0;
	for (char c : s) {
		value += Numeral(c).as_int();
		if (last && Numeral(c) > Numeral(last))
			value -= 2 * Numeral(last).as_int();
		last = c;
	}
}

//------------------------------------------------------------------------------

int Roman_int::as_int (void) const
{
	return value;
}

//------------------------------------------------------------------------------

istream& operator >> (istream& is, Roman_int& r)
{
}

//------------------------------------------------------------------------------

ostream& operator << (ostream& os, const Roman_int& r)
{
}

//------------------------------------------------------------------------------

