#include "main.h"

using namespace std;

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	string s;
	vector<string> dict;
	while (cin >> s) {
		bool is_short = false;
		for (int i = 0; i < s.size(); ++i) {
			if (ispunct(s[i])) {
				if (s[i] == '\'')
					is_short = true;
				else
					s[i] = ' ';
			} else {
				s[i] = tolower(s[i]);
			}
		}
		if (is_short) {
			if (s == "don't")
				s = "do not";
			else if (s == "doesn't")
				s = "does not";
			else if (s == "can't")
				s = "cannot";
			else if (s == "couldn't")
				s = "could not";
			else if (s == "won't")
				s = "will not";
			else if (s == "wouldn't")
				s = "would not";
			else for (int i = 0; i < s.length(); ++i) {
				if (s[i] == '\'')
					s[i] = ' ';
			}
		}
		// Remove header and trailing spaces.
		// -
		for (int i = 0; i < s.size() && isspace(s[i]); ++i)
			s.erase(i--);
		for (int i = s.size() - 1; i >= 0 && isspace(s[i]); --i)
			s.erase(i);
		dict.push_back(s);
	}
	sort(dict.begin(), dict.end());
	// Remove dublicates.
	// -
	auto last = unique(dict.begin(), dict.end());
	dict.erase(last, dict.end());
	for (string s : dict)
		cout << s << endl;
	return 0;
} catch (exception& e)
{
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
