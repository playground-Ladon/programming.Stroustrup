#include "main.h"

using namespace std;

// ----------------------------------- 80 ----------------------------------- //

int main (int argc, char* argv[])
try
{
	ifstream is("data.in");
	ofstream os("data.out");
	double d;
	map<double,int> m;
	while (is >> d) {
		++m[d];
	}
	for (auto i : m)
		cout << setw(16) << i.first << setw(16) << i.second << endl;
	return 0;
} catch (exception& e)
{
	cerr << "Exception occured!!!" << endl;
	cerr << " \\_____>  \"" << e.what() << "\"" << endl;
	return 1;
} catch (...)
{
	cerr << "Unknown exception occured!!!" << endl;
	return -1;
}
