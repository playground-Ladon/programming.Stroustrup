#include "main.h"

using namespace Graph_lib;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

int main (void)
{
	srand(time(NULL));
	// Top left corner, to start drawing.
	// -
	Point tl;

	// Draw a window.
	// -
	const int window_w = 600;
	const int window_h = 600;
	tl = Point(100,100);
	ex win(tl, window_w, window_h, "my window");

	// Draw.
	// -
	return gui_main();
}
