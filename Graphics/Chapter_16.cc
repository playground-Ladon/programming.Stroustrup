#include "Chapter_16.h"

using namespace std;
using namespace Graph_lib;

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

My_window::My_window (const Point& xy, const short unsigned int& w,
                      const short unsigned int& h, const string& title) :
	Window(xy,w,h,title), button_width(70), button_height(20),
	button_next(Point(x_max() - 70, 0), 70, 20, "Next", cb_next),
	button_quit(Point(x_max() - 70, 20), 70, 20, "Quit", cb_quit)
{
	attach(button_next);
	attach(button_quit);

}

short unsigned int My_window::x_max_inner (void) const
{
	int x = x_max() - button_width;
	if (x > 0)
		return x;
	return 0;
}

short unsigned int My_window::y_max_inner (void) const
{
	return y_max();
}

void My_window::cb_next (Address, Address pw)
{
	reference_to<My_window>(pw).next();
}

void My_window::next (void)
{
	redraw();
}

void My_window::cb_quit(Address, Address pw)
{
	reference_to<My_window>(pw).quit();
}

void My_window::quit()
{
	hide();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

ex::ex (const Point& xy, const short unsigned int& w,
	    const short unsigned int& h, const string& title)
			    : My_window(xy, w, h, title),
			      mid(x_max_inner() / 2.0, y_max_inner() / 2.0),
			      radius(min(x_max_inner(), y_max_inner()) / 2.0),
			      fps(30)
{
	// Add the airplane.
	// -
	airplane_init();
	airplane_draw();
}

void ex::airplane_init (void)
{
	Shape* part;
	unsigned char w = 50;
	unsigned char h = w;
	// Front wings (x, y), (x' ,y').
	// Back wings (x,y), (x', y').
	// -
	float pos[4][2] = {{w / 4.0F, 0},	{- w / 4.0F, w / 2.0F},
			   {- w / 2.0F, 0},	{- 3 * w / 4.0F, w / 4.0F}};
	// Draw the main body.
	// -
	part = new Ellipse(Point(0,0),w / 2.0, h / 7.0);
	airplane.push_back(part);
	// Draw the front upper wing.
	// -
	part = new Line(Point(pos[0][0], pos[0][1]),
			Point(pos[1][0], -pos[1][1]));
	airplane.push_back(part);
	// Draw the front lower wing.
	// -
	part = new Line(Point(pos[0][0], pos[0][1]),
			Point(pos[1][0], pos[1][1]));
	airplane.push_back(part);
	// Draw the back upper wing.
	// -
	part = new Line(Point(pos[2][0], pos[2][1]),
			Point(pos[3][0], -pos[3][1]));
	airplane.push_back(part);
	// Draw the back lower wing.
	// -
	part = new Line(Point(pos[2][0], pos[2][1]),
			Point(pos[3][0], pos[3][1]));
	airplane.push_back(part);

	// Move plane to initial position.
	// -
	const char x_start(100);
	for (char i = 0; i < airplane.size(); ++i)
		airplane[i].move(x_start, y_max() / 2.0);
}

void ex::airplane_draw (void)
{
	for (char i = 0; i < airplane.size(); ++i) {
		attach(airplane[i]);
	}
}

void ex::airplane_move (void)
{
	const char dx = 1;
	for (char i = 0; i < airplane.size(); ++i) {
		airplane[i].move(dx,0);
	}
}

bool ex::airplane_goal (void)
{
	return airplane[0].point(0).x >= x_max_inner() - 100;
}

void ex::next (void)
{
	static bool pushed(false);
	if (pushed) {
		pushed = false;
		Fl::remove_timeout(cb_fly,(void*) this);
	} else {
		pushed = true;
		Fl::add_timeout(1.0 / fps, cb_fly, (void*)this);
	}
}

void ex::cb_fly (void *pw)
{
	// Copied from :
	// https://stackoverflow.com/questions/14565470/force-fltk-to-redraw-without-event-handles
	// ... which in turn points to :
	// http://seriss.com/people/erco/fltk/#OpenGlInterp
	// -
	ex *pb = (ex*)pw;
	if (pb->airplane_goal()) {
		Fl::remove_timeout(cb_fly, pw);
	} else {
		pb->airplane_move();
		pb->redraw();
		Fl::repeat_timeout(1.0 / pb->fps, cb_fly, pw);
	}
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

// Notes.
// -
/*
static void cb_fly (void *userdata);

Fl::add_timeout(1.0 / fps, cb_fly, (void*)this);

void ex::cb_fly (void *pw)
{
	// Copied from :
	// https://stackoverflow.com/questions/14565470/force-fltk-to-redraw-without-event-handles
	// ... which in turn points to :
	// http://seriss.com/people/erco/fltk/#OpenGlInterp
	// -
	ex *pb = (ex*)pw;
	pb->redraw();
	Fl::repeat_timeout(1.0 / pb->fps, cb_fly, pw);
}
*/

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
