#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <set>
#include <unordered_map>

#include "Graph.h"
#include "GUI.h"
#include "Window.h"
#include "Point.h"
#include "Simple_window.h"

#include "Chapter_16.h"
