// Custom_Classes.h
// -

#include <algorithm>
// #include <cstring>
#include <FL/fl_draw.H>
#include <iostream>
#include <list>
#include "Point.h"
#include "Graph.h"

//------------------------------------------------------------------------------

typedef const Point& cpr;
typedef const unsigned char& cu8r;
typedef const unsigned char cu8;
typedef unsigned char u8;
typedef const unsigned short& cu16r;
typedef const unsigned short cu16;
typedef unsigned short u16;
typedef const unsigned int& cu32r;
typedef const unsigned int cu32;
typedef unsigned int u32;
typedef const string& csr;

//------------------------------------------------------------------------------

namespace Graph_lib
{

//------------------------------------------------------------------------------

class Arc : public Shape
{
	int w;
	int h;
	double a,phase;
public:
	Arc(Point p, int w, int h, double a) : Arc(p,w,h,a,0) { }
	Arc(Point p, int w, int h,
	    double a, double phase)    // center, min, and max distance from center
		: w(w), h(h), a(a), phase(-phase)
	{
		add(Point(p.x-w,p.y-h));
	}

	void draw_lines() const;

	Point center() const { return Point(point(0).x+w,point(0).y+h); }
	Point focus1() const { return Point(center().x+int(sqrt(double(w*w-h*h))),center().y); }
	Point focus2() const { return Point(center().x-int(sqrt(double(w*w-h*h))),center().y); }

	void set_major(int ww) { w=ww; }
	int major() const { return w; }
	void set_minor(int hh) { h=hh; }
	int minor() const { return h; }
};

//------------------------------------------------------------------------------

class Arrow : public Shape
{
	Point p[2];
public:
	Arrow (Point p1, Point p2);
	void draw_lines() const;
};

//------------------------------------------------------------------------------

class Binary_tree : public Shape {
protected:
Point tl;
// Local variables.
// -
// Cell radius, relative to tree height.
// -
double cell_r;
// Space between cells, relative to tree width.
// Cell superimposing, allowed.
// -
double cell_space;
// Level height, relative to cell radius.
// -
double level_h;
// Visual variables.
// -
Vector_ref<Shape> lines;
Vector_ref<Point> centers;
Vector_ref<Shape> cells;
public:
	Binary_tree (void) : Binary_tree(Point(100,100),400,150,3) { }
	Binary_tree (cu8r levels) : Binary_tree(Point(100,100),400,150,levels)
		{ }
	enum Line_type {normal, arrow};
	Binary_tree (cpr tl, cu16r width, cu16r height, cu8r levels) : 
		Binary_tree(tl,width,height,levels,normal) { }
	Binary_tree (cpr tl, cu16r width, cu16r height, 
		     cu8r levels, const Line_type& line_type);
	
	virtual void add_cells (void);
	virtual void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Custom_binary_tree : public Binary_tree {
public:
	Custom_binary_tree (void) : Binary_tree() { }
	Custom_binary_tree (cu8r levels) : Binary_tree(levels) { }
	Custom_binary_tree (cpr tl, cu16r width, cu16r height, cu8r levels) :
		Binary_tree(tl,width,height,levels) { }

	void add_cells (void);
};

//------------------------------------------------------------------------------

class Box : public Shape
{
	int h;    // height
	int w;    // width
public:
	Box (void) : Box(Point(100,100),300,100) { }
	Box(Point xy, int ww, int hh) : w(ww), h(hh)
	{
		add(xy);
		if (h<=0 || w<=0) error("Bad rectangle: non-positive side");
	}

	Box(Point x, Point y) : w(y.x-x.x), h(y.y-x.y)
	{
		add(x);
		if (h<=0 || w<=0) error("Bad rectangle: non-positive width or height");
	}
	void draw_lines() const;

	int height() const { return h; }
	int width() const { return w; }
};

//------------------------------------------------------------------------------

class Pseudo_window : public Box {
Point tl;
u16 width;
Line* title_line;
Text* title_text;
string title;
cu8 title_h;
double title_space;
public:
	Pseudo_window (void) : Pseudo_window(Point(100,100), 400, 
					     150, "No title.") { }

	Pseudo_window (const Point& tl, cu16r width, cu16r height) :
		Pseudo_window (tl,width,height,"No title.") { }

	Pseudo_window (const Point& tl, cu16r width, cu16r height, csr title);

	void set_title (csr title);
	
	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class My_box : public Shape
{
	vector<string> line;
	Font font;
	int font_size;
	double w,h;
public:
	My_box(Point xy, const string& text);

	void draw_lines() const;

	double width (void) { return w; }
	double height (void) { return h; }
	Point center (void) { return Point(point(0).x + w / 2.0, point(0).y + h / 2.0); }
};

//------------------------------------------------------------------------------

class Grid : public Lines {
cu32 cells;
const Point tl;
cu16 side;
Vector_ref<Rectangle> squares;
public:
	Grid (void) : Grid(Point(0,0),480) { }
	Grid (const Point& tl, cu16r side) : Grid(tl,side,7) { }
	Grid (const Point& tl, cu16r side, cu32r cells);
	
	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

// class Iterator {
// public:
	// virtual double* next (void) = 0;
// };

// class List_iterator : public Iterator {
	// list<double>::iterator it;
	// list<double> l;
// public:
	// List_iterator (const list<double>& ll) : l(ll), it(l.begin()) { }

	// double* next (void)
	// {
		// double* result = 0;
		// if (it != l.end()) {
			// result = &*it;
			// ++it;
		// }
		// return result;
	// }
// };

// class Vector_iterator : public Iterator {
	// u32 index;
	// vector<double> v;
// public:
	// Vector_iterator (const vector<double>& v) : v(v), index(0) { }

	// double* next (void)
	// {
		// double* result = 0;
		// if (index < v.size()) {
			// result = &v[index];
			// ++index;
		// }
		// return result;
	// }
// };

// void print (Iterator& it)
// {
	// for (double* p = it.next(); p != 0; p = it.next()) {
		// cout << *p << endl;
	// }
// }
//------------------------------------------------------------------------------

class Smiley : public Circle {
	Vector_ref<Circle> eyes;
	Open_polyline mouth;
	bool smiling;
public:
	Smiley (const Point& center, const double& radius);
	Smiley (const Point& center, const double& radius, bool frown);

	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Checkers : public Grid {
// Hold a board in shapes[0] and a piece type in shapes[1].
// Place pieces automatically in accordance with the game "checkers".
// -
Vector_ref<Smiley> pieces;
cu8 cells;
double cell_w;
public:
	Checkers (void) : Checkers(Point(10,10), 480) { }
	Checkers (const Point& tl, cu16r side);
	
	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Hexagon : public Closed_polyline
{
public:
	Hexagon (Point center, double radius) : Hexagon(center,radius,0) { }

	Hexagon (Point center, double radius, double rad);

	void draw_lines(void) const;
};

//------------------------------------------------------------------------------

class Regular_polygon : public Closed_polyline
{
public:
	Regular_polygon (Point center, double radius, uchar corners);
};

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

class Octagon : public Regular_polygon
{
public:
	Octagon (const Point& center, const double& radius) :
		Regular_polygon(center,radius,8) { }
};

//------------------------------------------------------------------------------

class Poly : public Closed_polyline
{
public:
	Poly (Point center, Vector_ref<Point> corner);
	
	void draw_lines(void) const { Closed_polyline::draw_lines(); }
};

//------------------------------------------------------------------------------

class Right_triangle : public Closed_polyline
{
public:
	Right_triangle (Point middle, uint h, uint w) : 
				Right_triangle(middle,h,w,0) { }
	Right_triangle (Point middle, uint h, uint w, double rad) : 
				Right_triangle(middle,h,w,rad,false) { }
	Right_triangle (Point middle, uint h, uint w, 
			double rad, bool mirror);

	void draw_lines (void) const { Closed_polyline::draw_lines(); }
};


//------------------------------------------------------------------------------

class Frowny : public Smiley {
public:
	Frowny (const Point& center, const double& radius);

	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Hatter_smiling : public Smiley {
	double ratio;
	Ellipse* hat_base;
	Arc* hat_top;
public:
	Hatter_smiling (const Point& center, const double& radius);
	Hatter_smiling (const Point& center, 
			const double& radius, const bool& smiling);
	~Hatter_smiling ()
	{
		delete hat_base;
		delete hat_top;
	}

	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Hatter_frowning : public Hatter_smiling {
public:
	Hatter_frowning (const Point& center, const double& radius);
	
	void draw_lines (void) const;
};

//------------------------------------------------------------------------------

class Immobile_circle : public Circle {
public:
	Immobile_circle (const Point& center, const double& radius) :
		Circle(center,radius) { }
	
	void draw_lines (void) const
	{
		Circle::draw_lines();
	}
	
	void move(int dx, int dy) { }
};

//------------------------------------------------------------------------------

class Striped_rectangle : public Rectangle {
Lines stripes;
const unsigned stripes_total;
bool stripes_show;
public:
	Striped_rectangle (const Point& tl, 
			   const unsigned int& w, const unsigned int& h) :
		Striped_rectangle(tl,w,h,3) { }
	Striped_rectangle (const Point& tl, 
			   const unsigned int& w, const unsigned int& h, 
			   const unsigned int& stripes_total);

	void draw_lines (void) const;

	void set_fill_color (Color col)
	{
		stripes_show = (col.visibility() != 
				Color::invisible)?true:false;
	}
};

//------------------------------------------------------------------------------

class Striped_circle : public Circle {
Vector_ref<Circle> stripes;
bool stripes_show;
const unsigned stripes_total;
Line_style stripes_style;
public:
	Striped_circle (const Point& center, const double& radius) :
		Striped_circle(center,radius,3) { }
	Striped_circle (const Point& center, 
			const double& radius, const unsigned int& stripes_total);

	void draw_lines (void) const;

	void set_fill_color (Color col)
	{
		stripes_show = (col.visibility() != 
				Color::invisible)?true:false;
	}
};

//------------------------------------------------------------------------------

class Striped_closed_polyline : public Closed_polyline {
	vector<Closed_polyline*> stripes;
	const unsigned stripes_total;

	void clear_stripes (void);
	void create_stripes (void);
public:
	Striped_closed_polyline (void) : Striped_closed_polyline(3) { }
	Striped_closed_polyline (const unsigned int& stripes_total);

	void add(Point p)
	{
		Shape::add(p);
	}

	void draw_lines (void) const;

	void update_stripes (void)
	{
		clear_stripes();
		create_stripes();
	}
	
	~Striped_closed_polyline (void)
	{
		clear_stripes();
	}
};

//------------------------------------------------------------------------------

// Rectangle functions.
// -

Point n (const Box& r);
Point s (const Box& r);
Point e (const Box& r);
Point w (const Box& r);
Point center (const Box& r);
Point ne (const Box& r);
Point se (const Box& r);
Point nw (const Box& r);
Point sw (const Box& r);

// Ellipse.
// -
Point n (const Ellipse& e);
Point s (const Ellipse& e);
Point e (const Ellipse& e);
Point w (const Ellipse& e);
Point center (const Ellipse& e);
Point ne (const Ellipse& e);
Point se (const Ellipse& e);
Point nw (const Ellipse& e);
Point sw (const Ellipse& e);

// Circle.
// -
Point n (const Circle& c);
Point s (const Circle& c);
Point e (const Circle& c);
Point w (const Circle& c);
Point center (const Circle& c);
Point ne (const Circle& c);
Point se (const Circle& c);
Point nw (const Circle& c);
Point sw (const Circle& c);

//------------------------------------------------------------------------------

} // Graph_lib
