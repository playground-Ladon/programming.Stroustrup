#include "Custom_Classes.h"

using namespace std;

namespace Graph_lib
{

void Arc::draw_lines() const
{
	if (color().visibility()) {
		double start,end;
		if (a > 0) {
			start = -(a - 90);
			end = 90;
		} else if (a < 0) {
			start = 90;
			end = 90 + a;
		} else {
			return;
		}
		fl_arc(point(0).x,point(0).y,w+w,h+h,start + phase,end + phase);
	}
}

//------------------------------------------------------------------------------

Arrow::Arrow (Point p1, Point p2) : p{p1, p2} { }

void Arrow::draw_lines () const
{
	// cout << p[0].x << endl;
	// cout << p[0].y << endl;
	// cout << p[1].x << endl;
	// cout << p[1].y << endl;
	// cout << "(" << p[0].x << "," << p[0].y << ") ---> (" << p[1].x << "," << p[1].y << ")" << endl;
	// Draw the main line.
	// -
	fl_line(p[0].x, p[0].y, p[1].x, p[1].y);

	// Verify.
	// -
	// cout << "a = " << a << endl;
	// Tip length as a total length percentage.
	// -
	double r = 0.20;
	// Total length.
	double l = sqrt(pow(p[1].x - p[0].x, 2) +
	                pow(p[1].y - p[0].y,2));

	// Tip starting position.
	// -
	const auto dx = p[1].x - p[0].x;
	const auto dy = p[1].y - p[0].y;

	// Sine, cosine; used with tip width.
	// -
	double sine = dy / l;
	double cosine = dx / l;

	// Tip size.
	// -
	const double ratio = 0.20;
	const double tip_w = ratio * l;

	Point tip_start(p[1].x - ratio * dx, p[1].y - ratio * dy);
	Point tip_sides[2] = { Point(tip_start.x - tip_w / 2 * sine,
	                             tip_start.y + tip_w / 2 * cosine),
	                       Point(tip_start.x + tip_w / 2 * sine,
	                             tip_start.y - tip_w / 2 * cosine)
	                     };

	// Draw tip sides.
	// -
	fl_line(tip_sides[0].x, tip_sides[0].y, p[1].x, p[1].y);
	fl_line(tip_sides[1].x, tip_sides[1].y, p[1].x, p[1].y);
}

//------------------------------------------------------------------------------

Binary_tree::Binary_tree (cpr tl, cu16r width, cu16r height, 
			  cu8r levels, const Line_type& line_type) :
	tl(tl)
{
	if (!levels)
		return;
	// Calculate cell_r.
	// level_h = 4 * cell_d
	// -
	// cell_d * levels + level_h * (levels - 1) = height
	// 			...
	// cell_d * (5 * levels - 1) = height
	// -
	cell_r = height / (2.0 * (5 * levels - 4));
	level_h = 8 * cell_r;
	// Calculate cell_space.
	// -
//	bottom cells #= 2 ^ (levels - 1)
//	cell_space * (bottom_cells - 1) + cell_d * bottom_cells = width
//	cell_space = (width - cell_d * bottom_cells) / (bottom_cells - 1)
	u32 bottom_cells = pow(2, levels - 1);
	cell_space = (width - 2 * cell_r * bottom_cells) / (bottom_cells - 1);
	
	// Calculate cell centers.
	// -
	double y = cell_r;
	for (u8 l = 0; l < levels; ++l, y += level_h + 2 * cell_r) {
		// Cell starting position.
		// -
		double x_step = width / pow(2, l + 1);
		// Cell position, relative to tree's left side.
		// -
		for (double x = x_step; x <= width - x_step; x += 2 * x_step) {
			Point p(tl.x + x, tl.y + y);
			centers.push_back(new Point(p));
		}
	}

	// Add lines.
	// -
	for (u8 c = 0, e = 1; e < centers.size(); ++c) {
		Point p1,p2,p3;
		p1 = centers[c];
		p2 = centers[e++];
		p3 = centers[e++];
		switch(line_type) {
		case normal:
			lines.push_back(new Line(p1, p2));
			lines.push_back(new Line(p1, p3));
			break;
		case arrow:
			lines.push_back(new Arrow(p1, p2));
			lines.push_back(new Arrow(p1, p3));
			break;
		}
	}
}

//------------------------------------------------------------------------------

void Binary_tree::add_cells (void)
{
	for (u32 u = 0; u < centers.size(); ++u) {
		cells.push_back(new Circle(centers[u],cell_r));
	}
	cout << "Original add_cells() called!" << endl;
}

//------------------------------------------------------------------------------

void Binary_tree::draw_lines (void) const
{
	for (u32 u = 0; u < cells.size(); ++u) {
		cells[u].draw();
	}
	for (u32 u = 0; u < lines.size(); ++u) {
		lines[u].draw();
	}
}

//------------------------------------------------------------------------------

void Custom_binary_tree::add_cells (void)
{
	for (u32 u = 0; u < centers.size(); ++u) {
		cells.push_back(new Hexagon(centers[u],cell_r));
	}
	cout << "New add_cells() called!" << endl;
}

//------------------------------------------------------------------------------

void Box::draw_lines() const
{
	// if (fill_color().visibility()) {    // fill
	// fl_color(fill_color().as_int());
	// fl_rectf(point(0).x,point(0).y,w,h);
	// }

	const int rx = 0.10 * w;
	const int ry = 0.10 * h;
	// const int& ry = rx;
	const int& x = point(0).x;
	const int& y = point(0).y;
	if (color().visibility()) {    // lines on top of fill
		fl_color(color().as_int());
		double p = 90;
		// Top left corner.
		// -
		fl_arc(x,y,2*rx,2*ry,0 + p,90 + p);
		fl_line(x + rx, y, x + w - rx, y);
		// Top right corner.
		// -
		p = 0;
		fl_arc(x+w-2*rx,y,2*rx,2*ry,0 + p,90 + p);
		fl_line(x + rx, y + h, x + w - rx, y + h);
		// Bottom left corner.
		// -
		p = 180;
		fl_arc(x,y+h-2*ry,2*rx,2*ry,0 + p,90 + p);
		fl_line(x, y + ry, x, y + h - ry);
		// Bottom right corner.
		// -
		p = -90;
		fl_arc(x+w-2*rx,y+h-2*ry,2*rx,2*ry,0 + p,90 + p);
		fl_line(x + w, y + ry, x + w, y + h - ry);
	}
}

//------------------------------------------------------------------------------

My_box::My_box (Point xy, const string& text) : font(Font::screen),
	font_size(16)
{
	add(xy);

	// Calculate maximum line width, through font size and character count.
	// -
	int old_font = fl_font();
	int old_font_size = fl_size();

	// Set font temporarily.
	// -
	fl_font(font.as_int(),font_size);

	// Store lines and calculate maximum character count.
	// -
	istringstream is(text);
	string l;
	double max_w = 0;
	for (double w; getline(is, l, '\n');) {
		w = fl_width(l.c_str());
		if (max_w < w)
			max_w = w;
		line.push_back(l);
	}

	// Add a whole character, '_', to maximum width.
	// -
	max_w += fl_width('_');

	// Update dimensions.
	// -
	w = max_w;
	h = (line.size() + 1) * font_size;

	// Restore old font.
	// -
	fl_font(old_font, old_font_size);
}

//------------------------------------------------------------------------------

Pseudo_window::Pseudo_window (const Point& tl, cu16r width, cu16r height, 
			      csr title) : tl(tl), width(width),
			      Box(tl,width,height), title_line(nullptr), 
			      title_text(nullptr), title(title), title_h(16)
{
	title_space = title_h * 0.2;
	Point p1(tl.x, tl.y + title_h + title_space);
	Point p2(tl.x + width, tl.y + title_h + title_space);
	title_line = new Line(p1, p2);
	set_title(title);
}

void Pseudo_window::set_title (csr title)
{
	// Calculate maximum line width, through font height and character count.
	// -
	int old_font = fl_font();
	int old_font_size = fl_size();

	// Set font temporarily.
	// -
	Font title_font(Font::screen);
	fl_font(title_font.as_int(),title_h);
	
	double title_w = fl_width(title.c_str());
	
	Point pos(tl.x + width/ 2.0 - title_w / 2, tl.y + 
						   title_h + title_space/2);
	if (title_text != nullptr)
		delete title_text;
	title_text = new Text(pos, title);
	title_text->set_font(title_font);
	title_text->set_font_size(title_h);

	fl_font(old_font, old_font_size);
}

void Pseudo_window::draw_lines (void) const
{
	Box::draw_lines();
	title_line->draw();
	title_text->draw_lines();
}

//------------------------------------------------------------------------------

void My_box::draw_lines (void) const
{
	// Draw rectangle.
	// -
	fl_color(color().as_int());
	fl_rect(point(0).x, point(0).y, w, h);

	// Save old font configuration.
	// -
	int old_font = fl_font();
	int old_font_size = fl_size();

	// Set our font.
	// -
	fl_font(font.as_int(),font_size);

	// Draw text.
	// Add half a char, '_', to all sides.
	// -
	for (int i = 0, text_x = point(0).x + fl_width('_') / 2, text_y;
	     i < line.size(); ++i) {
		text_y = point(0).y + (i + 1) * font_size + font_size / 2.0;
		fl_draw(line[i].c_str(), text_x, text_y);
	}

	// Restore old font.
	// -
	fl_font(old_font, old_font_size);
}

//------------------------------------------------------------------------------

Grid::Grid (const Point& tl, cu16r side, cu32r cells) :
	tl(tl), side(side), cells(cells)
{
	double cell_w = (double) side / cells;
	for (u8 i = 0; i < cells + 1; ++i) {
		double w = i * cell_w;
		// Add a vertical line.
		// -
		Point p1(tl.x + w, tl.y);
		Point p2(tl.x + w, tl.y + side - 1);
		Lines::add(p1,p2);
		// Add a horizontal line.
		// -
		Point p3(tl.x, tl.y + w);
		Point p4(tl.x + side - 1, tl.y + w);
		Lines::add(p3,p4);
	}
	Lines::set_style(Line_style(Line_style::solid,2));
	for (u8 i = 0; i < cells; ++i) {
		double w_i = i*cell_w;
		for (u8 j = 0; j <cells; ++j) {
			double w_j = j*cell_w;
			Point p(tl.x + w_i, tl.y + w_j);
			squares.push_back(new Rectangle(p,cell_w,cell_w));
			squares.back().set_color(Color::invisible);
			Color color(((i + j) % 2) ? Color::red : Color::white);
			squares.back().set_fill_color(color);
		}
	}
}

//------------------------------------------------------------------------------

void Grid::draw_lines (void) const
{
	Fl_Color oldc = fl_color();
	for (u32 i = 0; i < squares.size(); ++i) {
		fl_color(squares[i].color().as_int());
		squares[i].draw_lines();
	}
	fl_color(color().as_int());            // set color
	fl_line_style(style().style(),style().width()); // set style
	Lines::draw_lines();
	fl_color(oldc);      // reset color (to previous)
	fl_line_style(0);    // reset line style to default
}

//------------------------------------------------------------------------------

Checkers::Checkers (const Point& tl, cu16r side) : Grid(tl,side,8), cells(8),
						   cell_w((double) side/cells)
{
	// Add pieces.
	// -
	double piece_r = cell_w / 2 * 0.9;
	for (u8 j = 0; j < 3; ++j) {
		for (u8 i = j % 2; i < cells; i += 2) {
			Point pos(tl.x + cell_w/2 + i * cell_w,
				  tl.y + cell_w/2 + j * cell_w);
			pieces.push_back(new Smiley(pos,piece_r));
		}
	}
	for (u8 j = cells - 3; j < cells; ++j) {
		for (u8 i = ((j % 2)?1:0); i < cells; i += 2) {
			Point pos(tl.x + cell_w/2 + i * cell_w,
				  tl.y + cell_w/2 + j * cell_w);
			pieces.push_back(new Smiley(pos,piece_r));
		}
	}
}

void Checkers::draw_lines (void) const
{
	Grid::draw_lines();
	for (u8 i = 0; i < pieces.size(); ++i) {
		pieces[i].draw_lines();
	}
}

//------------------------------------------------------------------------------

Hexagon::Hexagon (Point center, double radius, double rad)
{
	for (double a = rad, step = 2 * M_PI / 6; a < 2 * M_PI + rad; a += step) {
		Closed_polyline::add(Point(center.x + sin(a) * radius,
		                           center.y - cos(a) * radius));
	}
}

//------------------------------------------------------------------------------

void Hexagon::draw_lines (void) const
{
	Closed_polyline::draw_lines();
}

//------------------------------------------------------------------------------

Regular_polygon::Regular_polygon (Point center, double radius, uchar corners)
{
	if (corners <= 2)
		corners = 3;
	for (double a = 0, step = 2 * M_PI / corners; a < 2 * M_PI; a += step) {
		Closed_polyline::add(Point(center.x + sin(a) * radius,
		                           center.y - cos(a) * radius));
	}
}

//------------------------------------------------------------------------------

Smiley::Smiley (const Point& center,
                const double& radius) : Smiley(center,radius,true) { }
Smiley::Smiley (const Point& center, const double& radius,
                bool smiling) : Circle(center,radius), smiling(true)
{
	// Add eyes
	// -
	double eye_r = radius / 8;
	double eye_R = 3 * radius / 5;
	double angle = M_PI / 4;
	Point eye[2] = {Point(center.x - eye_R * cos(angle),
	                      center.y - eye_R * sin(angle)),
	                Point(center.x + eye_R * cos(angle),
	                      center.y - eye_R * sin(angle))
	               };
	double pupil_r = eye_r / 2;
	Point pupil[2] = {Point(center.x - (eye_R - pupil_r / 4) * cos(angle),
	                        center.y - (eye_R - pupil_r / 4) * sin(angle)),
	                  Point(center.x + (eye_R - pupil_r / 4) * cos(angle),
	                        center.y - (eye_R - pupil_r / 4) * sin(angle))
	                 };
	for (int i = 0; i < 2; ++i) {
		eyes.push_back(new Circle(eye[i], eye_r));
		eyes.push_back(new Circle(pupil[i], pupil_r));
	}

	// Add mouth.
	// -
	double mouth_r = 2 * radius / 3;
	int points = 11;
	if (smiling)
		for (double a = -150.0 / 360 * 2 * M_PI,
		     step = (180.0 - 2 * 30) / points / 360 * 2 * M_PI;
		     a < -30.0 / 360 * 2 * M_PI + step; a += step) {
			mouth.add(Point(center.x + mouth_r * cos(a),
			                center.y - mouth_r * sin(a)));
		} else
		for (double a = 150.0 / 360 * 2 * M_PI,
		     step = (180.0 - 2 * 30) / points / 360 * 2 * M_PI;
		     a > 30.0 / 360 * 2 * M_PI - step; a -= step) {
			mouth.add(Point(center.x + mouth_r * cos(a),
			                center.y + radius - mouth_r * sin(a)));
		}
}

//------------------------------------------------------------------------------

void Smiley::draw_lines (void) const
{
	// Draw face.
	// -
	Circle::draw_lines();

	// Draw eyes.
	// -
	for (int i = 0; i < eyes.size(); ++i) {
		eyes[i].draw_lines();
	}
	mouth.draw_lines();
}

//------------------------------------------------------------------------------

Frowny::Frowny (const Point& center,
                const double& radius) : Smiley(center, radius, false) { }

//------------------------------------------------------------------------------

void Frowny::draw_lines (void) const
{
	Smiley::draw_lines();
}

//------------------------------------------------------------------------------

Hatter_smiling::Hatter_smiling (const Point& center, const double& radius) :
	Hatter_smiling::Hatter_smiling(center,radius,true)
{
}

//------------------------------------------------------------------------------

Hatter_smiling::Hatter_smiling (const Point& center,
                                const double& radius, const bool& smiling) :
	Smiley(center,radius*0.8,smiling), ratio(0.8)
{
	double hat_w = radius * ratio * 0.8;
	double hat_h = hat_w * 0.33;
	Point hat_center(center.x,center.y - radius*ratio);
	hat_base = new Ellipse(hat_center, hat_w, hat_h);
	hat_top = new Arc(hat_center, hat_h, hat_w, 180,-90);
}

//------------------------------------------------------------------------------

void Hatter_smiling::draw_lines (void) const
{
	Smiley::draw_lines();

	hat_base->draw_lines();
	hat_top->draw_lines();
}

//------------------------------------------------------------------------------

Hatter_frowning::Hatter_frowning (const Point& center, const double& radius) :
	Hatter_smiling(center,radius,false)
{
}

//------------------------------------------------------------------------------

void Hatter_frowning::draw_lines (void) const
{
	Hatter_smiling::draw_lines();
}

//------------------------------------------------------------------------------

Striped_rectangle::Striped_rectangle (const Point& tl, const unsigned int& w, 
	const unsigned int& h, const unsigned int& stripes_total) : 
	Rectangle(tl,w,h), stripes_show(true), stripes_total(stripes_total)
{
	const double stripes_w = h / stripes_total;
	for (double i = stripes_w / 2;
	     i <= h - stripes_w / 2; i += 2 * stripes_w) {
		Point p1(tl.x, tl.y + i);
		Point p2(tl.x + w - 1, tl.y + i);
		stripes.add(p1,p2);
	}
	stripes.set_style(Line_style(Line_style::solid,stripes_w));
}

//------------------------------------------------------------------------------

void Striped_rectangle::draw_lines (void) const
{
	Rectangle::draw_lines();
	if (stripes_show) {
		fl_line_style(stripes.style().style(),stripes.style().width());
		stripes.draw_lines();
		fl_line_style(0);
	}
}

//------------------------------------------------------------------------------

Striped_circle::Striped_circle (const Point& center, const double& radius, 
				const unsigned int& stripes_total) :
	Circle(center,radius), stripes_show(true), stripes_total(stripes_total),
	stripes_style(Line_style::solid)
{
	const double stripes_w = radius / stripes_total;
	stripes_style = Line_style(Line_style::solid,stripes_w);
	for (double r = 3 * stripes_w / 2;
	     r <= radius - stripes_w / 2; r += 2 * stripes_w) {
		stripes.push_back(new Circle(center,r));
	}
}

//------------------------------------------------------------------------------

void Striped_circle::draw_lines (void) const
{
	Circle::draw_lines();
	if (stripes_show) {
		fl_line_style(stripes_style.style(),stripes_style.width());
		for (unsigned int i = 0; i < stripes.size(); ++i) {
			stripes[i].draw_lines();
		}
		fl_line_style(0);
	}
}

//------------------------------------------------------------------------------

Striped_closed_polyline::Striped_closed_polyline
	(const unsigned int& stripes_total) : Closed_polyline(), 
	stripes_total(stripes_total)
{
}

//------------------------------------------------------------------------------

void Striped_closed_polyline::clear_stripes (void)
{
	for (unsigned int i = 0; i < stripes.size(); ++i)
		delete stripes[i];
	stripes.clear();
}

//------------------------------------------------------------------------------

void Striped_closed_polyline::create_stripes (void)
{
	// Find the center, arbitrarily, as an average.
	// -
	double avg_x = 0;
	double avg_y = 0;
	for (unsigned int i = 0; i < number_of_points(); ++i) {
		avg_x += point(i).x;
		avg_y += point(i).y;
	}
	avg_x /= number_of_points();
	avg_y /= number_of_points();
	Point center(avg_x, avg_y);

	for (double scaling_step = 1.0 / stripes_total, 
		    scaling_factor = scaling_step; 
	     scaling_factor <= 1 - scaling_step; 
	     scaling_factor += scaling_step) {
		stripes.push_back(new Closed_polyline);
		for (unsigned int j = 0; j < number_of_points(); ++j) {
			Point p(center.x + (point(j).x - center.x) * 
					     scaling_factor, 
				center.y + (point(j).y - center.y) *
					     scaling_factor);
			stripes.back()->add(p);
		}
	}
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void Striped_closed_polyline::draw_lines (void) const
{
	Closed_polyline::draw_lines();

	for (unsigned int i = 0; i < stripes.size(); ++i) {
		stripes[i]->draw_lines();
	}
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

// does two lines (p1,p2) and (p3,p4) intersect?
// if se return the distance of the intersect point as distances from p1
inline pair<double,double> line_intersect_clone(Point p1, Point p2, Point p3,
                Point p4, bool& parallel)
{
	double x1 = p1.x;
	double x2 = p2.x;
	double x3 = p3.x;
	double x4 = p4.x;
	double y1 = p1.y;
	double y2 = p2.y;
	double y3 = p3.y;
	double y4 = p4.y;

	double denom = ((y4 - y3)*(x2-x1) - (x4-x3)*(y2-y1));
	if (denom == 0) {
		parallel= true;
		return pair<double,double>(0,0);
	}
	parallel = false;
	return pair<double,double>( ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3))/denom,
	                            ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3))/denom);
}

//intersection between two line segments
//Returns true if the two segments intersect,
//in which case intersection is set to the point of intersection
bool line_segment_intersect_clone(Point p1, Point p2, Point p3, Point p4,
                                  Point& intersection)
{
	bool parallel;
	pair<double,double> u = line_intersect_clone(p1,p2,p3,p4,parallel);
	if (parallel || u.first < 0 || u.first > 1 || u.second < 0
	    || u.second > 1) return false;
	intersection.x = p1.x + u.first*(p2.x - p1.x);
	intersection.y = p1.y + u.first*(p2.y - p1.y);
	return true;
}

Poly::Poly (Point center, Vector_ref<Point> corner)
{
	if (corner.size() <= 2)
		error("Poly with < 3 corners!!!");

	for (int i = 1; i < 3; ++i) {
		if (corner[i] == corner[i-1])
			error("Poly: identical points detected!");
	}
	bool parallel;
	line_intersect_clone(corner[1],corner[2],corner[0],corner[1],parallel);
	if (parallel)
		error("two polygon points lie in a straight line");
	for (int i = 0; i < 3; ++i) {
		Closed_polyline::add(corner[i]);
	}
	for (int i = 3; i < corner.size(); ++i) {
		line_intersect_clone(corner[i-1],corner[i],corner[i-2],corner[i-1],parallel);
		if (parallel)
			error("two polygon points lie in a straight line");
		for (int j = 1; j < i - 1; ++j) {
			Point ignore(0,0);
			if (line_segment_intersect_clone(corner[i-1],corner[i],corner[j-1],corner[j],
			                                 ignore))
				error("intersect in polygon");
		}
		Closed_polyline::add(corner[i]);
	}

	// int np = number_of_points();

	// if (1<np) {    // check that thenew line isn't parallel to the previous one
	// if (p==point(np-1)) error("polygon point equal to previous point");
	// bool parallel;
	// line_intersect(point(np-1),p,point(np-2),point(np-1),parallel);
	// if (parallel)
	// error("two polygon points lie in a straight line");
	// }

	// for (int i = 1; i<np-1; ++i) {    // check that new segment doesn't interset and old point
	// Point ignore(0,0);
	// if (line_segment_intersect(point(np-1),p,point(i-1),point(i),ignore))
	// error("intersect in polygon");
	// }


	// Closed_polyline::add(p);
}

//------------------------------------------------------------------------------

Right_triangle::Right_triangle (Point middle, uint h, uint w,
                                double rad, bool mirror)
{
	Point top(middle.x + h * sin(rad), middle.y - h * cos(rad));
	Point right(middle.x + w * cos(rad), middle.y + w * sin(rad));

	if (mirror == true) {
		Point tmp = top;
		top = right;
		right = tmp;
	}

	// Draw.
	// -
	Closed_polyline::add(top);
	Closed_polyline::add(middle);
	Closed_polyline::add(right);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

Point n (const Box& r)
{
	return Point(r.point(0).x + r.width() / 2.0, r.point(0).y);
}

Point s (const Box& r)
{
	return Point(r.point(0).x + r.width() / 2.0, r.point(0).y + r.height());
}

Point e (const Box& r)
{
	return Point(r.point(0).x + r.width(), r.point(0).y + r.height() / 2.0);
}

Point w (const Box& r)
{
	return Point(r.point(0).x, r.point(0).y + r.height() / 2.0);
}

Point center (const Box& r)
{
	return Point(r.point(0).x + r.width() / 2.0, r.point(0).y + r.height() / 2.0);
}

Point ne (const Box& r)
{
	return Point(r.point(0).x + r.width(), r.point(0).y);
}

Point se (const Box& r)
{
	return Point(r.point(0).x + r.width(), r.point(0).y + r.height());
}

Point nw (const Box& r)
{
	return r.point(0);
}

Point sw (const Box& r)
{
	return Point(r.point(0).x, r.point(0).y + r.height());
}

//------------------------------------------------------------------------------

Point n (const Ellipse& e)
{
	return Point(e.point(0).x + e.major(), e.point(0).y);
}

Point s (const Ellipse& e)
{
	return Point(e.point(0).x + e.major(), e.point(0).y + 2.0 * e.minor());
}

Point e (const Ellipse& e)
{
	return Point(e.point(0).x + 2.0 * e.major(), e.point(0).y + e.minor());
}

Point w (const Ellipse& e)
{
	return Point(e.point(0).x, e.point(0).y + e.minor());
}

Point center (const Ellipse& e)
{
	return Point(e.point(0).x + e.major(), e.point(0).y + e.minor());
}

Point ne (const Ellipse& e)
{
	return Point(e.point(0).x + 2.0 * e.major(), e.point(0).y);
}

Point se (const Ellipse& e)
{
	return Point(e.point(0).x + 2.0 * e.major(), e.point(0).y + 2.0 * e.minor());
}

Point nw (const Ellipse& e)
{
	return e.point(0);
}

Point sw (const Ellipse& e)
{
	return Point(e.point(0).x, e.point(0).y + 2.0 * e.minor());
}

//------------------------------------------------------------------------------

Point n (const Circle& c)
{
	return Point(c.center().x, c.center().y - c.radius());
}

Point s (const Circle& c)
{
	return Point(c.center().x, c.center().y + c.radius());
}

Point e (const Circle& c)
{
	return Point(c.center().x + c.radius(), c.center().y);
}

Point w (const Circle& c)
{
	return Point(c.center().x - c.radius(), c.center().y);
}

Point center (const Circle& c)
{
	return c.center();
}

Point ne (const Circle& c)
{
	return Point(c.center().x + c.radius(), c.center().y - c.radius());
}

Point se (const Circle& c)
{
	return Point(c.center().x + c.radius(), c.center().y + c.radius());
}

Point nw (const Circle& c)
{
	return Point(c.center().x - c.radius(), c.center().y - c.radius());
}

Point sw (const Circle& c)
{
	return Point(c.center().x - c.radius(), c.center().y + c.radius());
}

//------------------------------------------------------------------------------

} // Graph_lib
