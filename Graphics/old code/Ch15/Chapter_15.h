#ifndef CHAPTER_15_GUARD
#define CHAPTER_15_GUARD 1

#include <iostream>
#include <vector>

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#include "Graph.h"
#include "Window.h"

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

namespace Graph_lib {

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
class XY_axis;

class Bargraph : public Shape {
	std::vector<double> v;
	double range[2];
	Point orig;
	double scale[2];
public:
	Bargraph (const unsigned char& n, const double& xmin, 
		  const double& xmax, const Point& oorig, 
		  const double& xscale = 25, const double& yscale = 25);
	Bargraph (const unsigned char& n, const XY_axis& xy);

	void graph (void);
};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

class XY_axis;

class Fct : public Shape {
	double (* f) (double);
	double range[2];
	Point orig;
	int points;
	double scale[2];
public:
	Fct (double (& ff) (double), const double& xmin, 
	     const double& xmax, const Point& oorig, 
	     const int& ppoints = 100, const double& xscale = 25, 
	     const double& yscale = 25);
	Fct (double (& ff) (double), const XY_axis& xy, 
	     const int& ppoints = 100);

	void graph (void);
	void set_function (double (& ff) (double));
	void set_points (const int& ppoints);
	void set_range (const double& xmin, const double& xmax);
};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

class XY_axis : public Shape {
const Point mid;
const unsigned short notches[2];
const unsigned short size;
Vector_ref<const Axis> xy;
public:
	XY_axis(const Window& win, const Point& mmid, 
		short unsigned int notches_x = 0, 
		short unsigned int notches_y = 0) 
			: size(min(win.x_max(), 
			       win.y_max())), mid(mmid), 
			       notches{notches_x, notches_y}
	{
		const Point origin_x(mid.x - size / 2.0, mid.y);
		const Point origin_y(mid.x, mid.y + size / 2.0);
		xy.push_back(new const Axis(Axis::x,origin_x,size,
					    notches[0],"x axis"));
		xy.push_back(new const Axis(Axis::y,origin_y,size,
					    notches[1],"y axis"));
	}
	
	void draw_lines (void) const
	{
		xy[0].draw_lines();
		xy[1].draw_lines();
	}
	
	double max_x (void) const
	{
		return notches[0] / 2.0;
	}

	double max_y (void) const
	{
		return notches[1] / 2.0;
	}

	double min_x (void) const
	{
		return - max_x();
	}

	double min_y (void) const
	{
		return min_x();
	}

	short unsigned int length (void) const
	{
		return size;
	}
	
	Point orig (void) const
	{
		return mid;
	}

	double scale_x (void) const
	{
		return size / (double) notches[0];
	}

	double scale_y (void) const
	{
		return size / (double) notches[1];
	}
};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

} // Graph_lib

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#endif
