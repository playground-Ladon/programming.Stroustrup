#include "Chapter_15.h"

using namespace std;
using namespace Graph_lib;

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

Bargraph::Bargraph (const unsigned char& n, 
		    const double& xmin, const double& xmax, 
		    const Point& oorig, const double& xscale, 
		    const double& yscale) : 
			v(n), range{xmin, xmax}, 
			orig(oorig), scale{xscale, yscale}
{
	// Check parameters.
	// -
	if (range[1] - range[0] <= 0)
		error("bad graphing range");
	if (!n)
		error("bargraph called with no bars!!");
	std::cout << "Enter " << (short) n << " data for the "
		     "bargraph : " << std::endl;
	
	// Read data.
	// -
	for (unsigned char i = 0; i < n; ++i) {
		std::cin >> v[i];
	}
	// Scale data to max_y.
	// -
	// Find maximum.
	// -
	unsigned char m = 0;
	for (unsigned char i = 1; i < v.size(); ++i) {
		if (fabs(v[i]) > fabs(v[m]))
			m = i;
	}
	// Scale data down to 1.
	// -
	// Store v[m] because it will change.
	// -
	const double divisor = fabs(v[m]);
	for (unsigned char i = 0; i < v.size(); ++i) {
		v[i] /= divisor;
	}
	
	graph();
}

Bargraph::Bargraph (const unsigned char& n, const XY_axis& xy) 
	: Bargraph(n, xy.min_x(), xy.max_x(), xy.orig(), 
		   xy.scale_x(), xy.scale_y()) { }

void Bargraph::graph (void)
{
	//
	// -
	// Total points = 1 for start, 2 for each bar, 1 for end.
	// -
	double bar_w = (range[1] - range[0]) / v.size();
	// Add first point.
	// -
	double r = range[0];
	add(Point(orig.x + int(r * scale[0]), orig.y));
	for (int i = 0; i < v.size(); ++i) {
		add(Point(orig.x + int(r * scale[0]), 
			  orig.y - int(v[i] * scale[1])));
		r += bar_w;
		add(Point(orig.x + int(r * scale[0]), 
			  orig.y - int(v[i] * scale[1])));
	}
	// Add last point.
	// -
	add(Point(orig.x + int(r * scale[0]), orig.y));
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

Fct::Fct (double (& ff)(double), const double& x_min, 
	  const double& x_max, const Point& oorig, 
	  const int& ppoints, const double& scale_x, 
	  const double& scale_y) : 
		f(&ff), range{x_min, x_max}, orig(oorig), 
		points(ppoints), scale{scale_x, scale_y}
{
	// cout << "x min, max = " << x_min << ", " << x_max << endl;
	// cout << "x range = " << range[0] << ", " << range[1] << endl;
	// cout << "scale_x, scale_y = " << scale_x << ", " << scale_y << endl;
	// cout << "scale = " << scale[0] << ", " << scale[1] << endl;
	if (range[1] - range[0] <= 0)
		error("bad graphing range");
	if (points <= 0)
		error("non-positive graphing points");
	graph();
}

Fct::Fct (double (& ff) (double), const XY_axis& xy, const int& ppoints)
	: Fct(ff, xy.min_x(), xy.max_x(), xy.orig(), 
	      ppoints, xy.scale_x(), xy.scale_y()) { }

void Fct::graph (void)
{
	double dist = (range[1] - range[0]) / points;
	double r = range[0];
	for (int i = 0; i <= points; ++i) {
		add(Point(orig.x + int(r * scale[0]), 
			  orig.y - int(f(r) * scale[1])));
		r += dist;
	}
}

void Fct::set_function (double (& ff) (double))
{
	f = &ff;
	// Delete all stored points (in Shape).
	// -
	clear();
	graph();
}

void Fct::set_points (const int& ppoints)
{
	points = ppoints;
	if (points <= 0)
		error("non-positive graphing points");
	clear();
	graph();
}

void Fct::set_range (const double& xmin, const double& xmax)
{
	range[0] = xmin;
	range[1] = xmax;
	if (range[1] - range[0] <= 0)
		error("bad graphing range");
	clear();
	graph();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
