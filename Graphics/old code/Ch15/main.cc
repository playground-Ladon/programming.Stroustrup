#include "main.h"

using namespace Graph_lib;

//------------------------------------------------------------------------------

double series_pi4th_term (const int& i) 
{
	return pow(-1, i) / (i * 2 + 1);
}

double series_pi4th (double i)
{
	double r = 0;
	for (int j = 0; j <= i; ++j) {
		r += series_pi4th_term(j);
	}
	return r;
}

double sum (double x)
{
	return sin(x) + cos(x);
}

double cos_2a (double x)
{
	return sin(x) * sin(x) + cos(x) * cos(x);
}

//------------------------------------------------------------------------------

int main (void)
{
	srand(time(NULL));
	// Top left corner, to start drawing.
	// -
	Point tl;

	// Draw a window.
	// -
	const int window_w = 600;
	const int window_h = 600;
	tl = Point(100,100);
	Graph_lib::Window win(tl, window_w, 
			      window_h, "Διαγράμματα συναρτήσεων");
	Point mid(window_w/2,window_h/2);
	double r = 2.0 * min(window_w,window_h)/5;
	
	// Draw a shape or execute some code.
	// -
	constexpr int axis_length = min(window_w, window_h) - 20;
	constexpr int xnotches = ceil(2 * M_PI);
	constexpr double ynotches = 2;
	XY_axis xy(win,mid,xnotches,ynotches);
	win.attach(xy);
	Fct ssin(sin, xy);
	win.attach(ssin);
	Bargraph bargraph(10,xy);
	win.attach(bargraph);

	// Draw and wait.
	// -
	gui_main();
	return 0;
}
