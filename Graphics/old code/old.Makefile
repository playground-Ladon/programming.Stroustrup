#
# This is a common Makefile for code examples from the book
# "Programming -- Principles and Practice Using C++" by Bjarne Stroustrup
#

#
# Usage:
#     make        - Build all examples
#     make clean  - Clean all examples
#

INCLUDES = -I"$(FLTK)" -I/usr/include/fltk -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libdrm -I/usr/include/libpng16
LIBS     = -lstdc++ -Wl,-rpath,/usr/lib64/fltk -lfltk -lXcursor -lXfixes -lXext -lXft -lfontconfig -lXinerama -lpthread -ldl -lm -lX11
CXXFLAGS = $(INCLUDES) -Wall -time -O3 -DNDEBUG -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT
LIBFLAGS = -L/usr/lib64/fltk
AR       = ar

.SUFFIXES: .cpp .o

# Create a list of source files.
SOURCES  = $(shell ls *.cpp)
# Create a list of object files from the source file lists.
OBJECTS = ${SOURCES:.cpp=.o}     
# Create a list of targets.
TARGETS = libbookgui.a main

# Build all targets by default
all: 	$(TARGETS)

%.a: $(OBJECTS)
	$(AR) rcs $@ $(OBJECTS)
main:	main.o Graph.o GUI.o Simple_window.o

# A rule to build .o file out of a .cpp file
%.o: %.cpp 
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# A rule to clean all the intermediates and targets
clean:	
	rm -rf $(TARGETS) $(OBJECTS) *.out *.stackdump
