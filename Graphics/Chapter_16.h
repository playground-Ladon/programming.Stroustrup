#ifndef CHAPTER_16_GUARD
#define CHAPTER_16_GUARD 1

#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <vector>

#include "Graph.h"
#include "GUI.h"
#include "Window.h"

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

namespace Graph_lib {

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

struct My_window : Window {
	My_window (const Point& xy,
		   const short unsigned int& w, const short unsigned int& h,
		   const string& title);
protected:
	short unsigned int x_max_inner (void) const;
	short unsigned int y_max_inner (void) const;
	const short unsigned int button_width;
	const short unsigned int button_height;
private:
	Button button_next;
	Button button_quit;

	static void cb_next(Address, Address);
	virtual void next (void);
	static void cb_quit(Address, Address);
	void quit (void);
};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

struct ex : My_window {
	ex (const Point& xy, const short unsigned int& w,
	      const short unsigned int& h, const string& title);
	void update_clock (void);
private:
	Point mid;
	float radius;
	unsigned char fps;
	Vector_ref<Shape> airplane;

	void airplane_init (void);
	void airplane_draw (void);
	void airplane_move (void);
	// Has the airplane reached the goal state?
	// -
	bool airplane_goal (void);
	// Start flying.
	// -
	void next (void);
	// Fly to goal.
	// -
	static void cb_fly (void *userdata);
};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

} // Graph_lib

#endif
