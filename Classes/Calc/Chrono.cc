
//
// This is example code from Chapter 9.8 "The Date class" of
// "Programming -- Principles and Practice Using C++" by Bjarne Stroustrup
//

#include "Chrono.h"

namespace Chrono
{

// member function definitions:

//------------------------------------------------------------------------------

int days_in_month (const Date::Month& m, const int& year);
Date::Month next_month (const Date::Month& month);

Date::Date(int yy, Month mm, int dd)
	: y(yy), m(mm), d(dd)
{
	if (!is_date(yy,mm,dd)) throw Invalid();
	// count days since 1 Jan 1970
	// -
	days = 0;
	// add years
	// -
	for (int i = 1970; i < yy; ++i) {
		days += 365;
		days += leapyear(i)?1:0;
	}
	// add months
	// -
	for (Date::Month i; i != mm; i = next_month(i)) {
		days += days_in_month(i,yy);
	}
	// add days
	// -
	days += dd;
}

//------------------------------------------------------------------------------

const Date& default_date()
{
	static const Date dd(2001,Date::jan,1); // start of 21st century
	return dd;
}

//------------------------------------------------------------------------------

Date::Date() : Date::Date(default_date().year(), default_date().month(),
	                          default_date().day()) { }

//------------------------------------------------------------------------------

int days_in_month (const Date::Month& m, const int& year);

void Date:: add_day(int n)
{
	// ...
	days += n;
	d += n;
	while (d > days_in_month(m,y)) {
		d -= days_in_month(m,y);
		m = next_month(m);
		if (m == jan) {
			++y;
		}
	}
}

//------------------------------------------------------------------------------

void Date::add_month(int n)
{
	while (n) {
		days += days_in_month(m,y);
		m = next_month(m);
		if (m == jan) {
			++y;
		}

		--n;
	}
}

//------------------------------------------------------------------------------

void Date::add_year(int n)
{
	if (m==feb && d==29 && !leapyear(y+n)) { // beware of leap years!
		m = mar;        // use March 1 instead of February 29
		d = 1;
	}
	y+=n;
}

//------------------------------------------------------------------------------

// helper functions:

bool is_date(int y, Date::Month  m, int d)
{
	// assume that y is valid

	if (d<=0) return false;            // d must be positive
	if (y < 1970) return false;

	int days_in_month = 31;            // most months have 31 days

	switch (m) {
	case Date::feb:                        // the length of February varies
		days_in_month = (leapyear(y))?29:28;
		break;
	case Date::apr:
	case Date::jun:
	case Date::sep:
	case Date::nov:
		days_in_month = 30;                // the rest have 30 days
		break;
	}

	if (days_in_month<d) return false;

	return true;
}

//------------------------------------------------------------------------------

bool leapyear(int y)
{
	if (y % 400 == 0)
		return true;
	if (y % 100 == 0)
		return false;
	if (y % 4 == 0)
		return true;
	return false;
}

//------------------------------------------------------------------------------

bool operator==(const Date& a, const Date& b)
{
	return a.year()==b.year()
	       && a.month()==b.month()
	       && a.day()==b.day();
}

//------------------------------------------------------------------------------

bool operator!=(const Date& a, const Date& b)
{
	return !(a==b);
}

//------------------------------------------------------------------------------

ostream& operator<<(ostream& os, const Date& d)
{
	return os << '(' << d.year()
	       << ',' << d.month()
	       << ',' << d.day()
	       << ')';
}

//------------------------------------------------------------------------------

istream& operator>>(istream& is, Date& dd)
{
	int y, m, d;
	char ch1, ch2, ch3, ch4;
	is >> ch1 >> y >> ch2 >> m >> ch3 >> d >> ch4;
	if (!is) return is;
	if (ch1!='(' || ch2!=',' || ch3!=',' || ch4!=')') { // oops: format error
		is.clear(ios_base::failbit);                    // set the fail bit
		return is;
	}
	dd = Date(y,Date::Month(m),d);     // update dd
	return is;
}

//------------------------------------------------------------------------------

enum Day {
	sunday, monday, tuesday, wednesday, thursday, friday, saturday
};

//------------------------------------------------------------------------------

unsigned char day_to_int (const Day& d)
{
	switch (d) {
	case sunday:
		return 0;
	case monday:
		return 1;
	case tuesday:
		return 2;
	case wednesday:
		return 3;
	case thursday:
		return 4;
	case friday:
		return 5;
	case saturday:
		return 6;
	}
}

Day int_to_day (const unsigned int& i)
{
	switch (i) {
	case 0:
		return sunday;
	case 1:
		return monday;
	case 2:
		return tuesday;
	case 3:
		return wednesday;
	case 4:
		return thursday;
	case 5:
		return friday;
	case 6:
		return saturday;
	}
}

//------------------------------------------------------------------------------

unsigned int total_days (const Date& date);

Day day_of_week(const Date& d)
{
	int day = total_days(d) % 7;
	return int_to_day(day);
}

//------------------------------------------------------------------------------

Date next_Sunday(const Date& d)
{
	Date result_date = d;
	Day day = day_of_week(d);
	int days_to_add = 0;
	switch (day) {
	case sunday:
		break;
	case monday:
		days_to_add = 6;
		break;
	case tuesday:
		days_to_add = 5;
		break;
	case wednesday:
		days_to_add = 4;
		break;
	case thursday:
		days_to_add = 3;
		break;
	case friday:
		days_to_add = 2;
		break;
	case saturday:
		days_to_add = 1;
		break;
	}
	result_date.add_day(days_to_add);
	return result_date;
}

//------------------------------------------------------------------------------

Date next_workday (const Date& date);

Date next_weekday(const Date& d)
{
	// ...
	return next_workday (d);
}

//------------------------------------------------------------------------------

int days_in_month (const Date::Month& m, const int& year)
{
	int days = 31;
	switch (m) {
	case Date::feb:                        // the length of February varies
		days = leapyear(year)?29:28;
		break;
	case Date::apr:
	case Date::jun:
	case Date::sep:
	case Date::nov:
		days = 30;                // the rest have 30 days
		break;
	}
	return days;
}

//------------------------------------------------------------------------------

Date::Month next_month (const Date::Month& month)
{
	switch (month) {
	case Date::jan :
		return Date::feb;
	case Date::feb :
		return Date::mar;
	case Date::mar :
		return Date::apr;
	case Date::apr :
		return Date::may;
	case Date::may :
		return Date::jun;
	case Date::jun :
		return Date::jul;
	case Date::jul :
		return Date::aug;
	case Date::aug :
		return Date::sep;
	case Date::sep :
		return Date::oct;
	case Date::oct :
		return Date::nov;
	case Date::nov :
		return Date::dec;
	case Date::dec :
		return Date::jan;
	}
}

//------------------------------------------------------------------------------

unsigned int total_days (const Date& date)
{
	unsigned int days = 0;
	Date::Month m = Date::jan;
	for (Date::Month m = Date::jan; m != date.month(); m = next_month(m)) {
		days += days_in_month (m,date.year());
	}
	return days;
}

//------------------------------------------------------------------------------

Date next_workday (const Date& date)
{
	// 1 Jan is Sunday
	// -
	unsigned int days = total_days(date);
	// total days
	// -
	days += date.day();
	// next day
	// -
	++days;
	// 0 or 6 for Sunday or Saturday
	// -
	days %= 7;
	// result
	// -
	Date workday = date;
	// Sunday
	// -
	if (days == 0)
		workday.add_day(1);
	// Saturday
	// -
	else if (days == 6)
		workday.add_day(2);
	return workday;
}

//------------------------------------------------------------------------------

} // Chrono

//------------------------------------------------------------------------------

/*int main()
try
{
    Chrono::Date holiday(1978, Chrono::Date::jul, 4); // initialization
    Chrono::Date d2 = Chrono::next_Sunday(holiday);
    Chrono::Day  d  = day_of_week(d2);
    cout << "holiday is " << holiday << " d2 is " << d2 << endl;
    return holiday != d2;
}
catch (Chrono::Date::Invalid&) {
    cerr << "error: Invalid date\n";
    return 1;
}
catch (...) {
    cerr << "Oops: unknown exception!\n";
    return 2;
}*/

//------------------------------------------------------------------------------
