#include <cstdio>
#include <iostream>
#include <string>

namespace Calc
{

//------------------------------------------------------------------------------

using std::ostream;
using std::string;

typedef unsigned long long int u64;

//------------------------------------------------------------------------------

class Invalid {
	string message;
public:
	Invalid (void);
	Invalid (const string& message);
	string what (void) { return message; }
};

//------------------------------------------------------------------------------

class Rational {
	u64 numerator;
	u64 denominator;

	u64 gcd (void);
	void reduce (void);
public:
	Rational (const u64& num, const u64& denom);
	Rational (void);

	u64 getnum (void) const;
	u64 getdenom (void) const;

	operator long double () const;

	Rational& operator= (const string& s);

	Rational operator- (void) const;
	Rational operator+ (const Rational& r) const;
	Rational operator- (const Rational& r) const;
	Rational operator* (const Rational& r) const;
	Rational operator/ (const Rational& r) const;
};

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const Rational& r);

//------------------------------------------------------------------------------

} // Calc
