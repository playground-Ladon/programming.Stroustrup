// AV Rule 37
//	   Header (include) files should include only those header files that are required for them to
// 	   successfully compile. Files that are only used by the associated .cpp file should be placed in
// 	   the .cpp file—not the .h file.

#include <algorithm>	// sort()
// #include <bitset>
// #include <cassert>
// #include <cctype>
// #include <cerrno>
// #include <cfenv>
// #include <chrono>
// #include <cmath>
// #include <complex>
// #include <cstdio>
#include <cstdlib>
// #include <cstring>
// #include <ctime>
// #include <experimental/random>	// randint()
// #include <fstream>
// #include <iomanip>
#include <iostream>
// #include <iterator>
// #include <limits>
// #include <list>
#include <map>
// #include <numeric>
// #include <random>
// #include <regex>
// #include <set>
#include <string>
// #include <tuple>
// #include <unordered_map>
// #include <vector>

#include "main.h"

using namespace std;

int main (void)
try
{
	srand(0);
	const int N = 5000000;
//	string** a = new string*[N];
	string s;
	map<string,char> m;
	for (int i = 0; i < N; ++i)
	{
		int r = rand() % 100;
		s.resize(r);
		for (int j = 0; j < r; ++j)
		{
			s[j] = rand() % 128;
		}
		m[s.c_str()] = '\0';
	}
//	sort(a, a + N);
}
catch (const exception& e)
{
	cout << "Exception : " << e.what() << endl;
}
catch (...)
{
	cout << "Unknown exception." << endl;
}
